///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

import {
  LinePrimitive,
  Point3,
  SceneEntity,
  SceneUpdate,
  TextPrimitive,
  Color,
} from "@foxglove/schemas";

import { Message } from "./apolloTypes";
import {
  convertPerceptionObstacle,
  convertTimestamp,
  //   get_min_coordinates,
} from "./perception_obstacles";

enum Priority {
  CAUTION = 1,
  NORMAL = 2,
  IGNORE = 3,
}

enum Interact {
  INTERACTION = 1,
  NONINTERACTION = 2,
}

function priority2color(t: Priority): Color {
  switch (t) {
    case Priority.IGNORE:
      return { r: 1, g: 1, b: 1, a: 0.3 };
    case Priority.NORMAL:
      return { r: 0, g: 1, b: 0, a: 0.3 };
    case Priority.CAUTION:
      return { r: 1, g: 0, b: 0, a: 1 };
    default:
      return { r: 1, g: 0, b: 1, a: 1 };
  }
}

function convertTrajectory(
  obstacle: Message<"apollo.prediction.PredictionObstacle">,
  offset: Point3
): LinePrimitive[] {
  let lines: LinePrimitive[] = [];

  for (let traj of obstacle.trajectory) {
    let points: Message<"foxglove.Point3">[] = [];
    for (let p of traj.trajectory_point) {
      points.push({
        x: p.path_point.x - offset.x,
        y: p.path_point.y - offset.y,
        z: obstacle.perception_obstacle.position.z - offset.z,
      });
    }

    let line: LinePrimitive = <LinePrimitive>{};

    line.type = 0;
    line.thickness = 0.3;
    line.scale_invariant = false;
    line.points = points;

    line.color = priority2color(obstacle.priority?.priority);

    line.pose = {
      position: offset,
      orientation: { x: 0, y: 0, z: 0, w: 1 },
    };
    lines.push(line);
  }

  return lines;
}

function appendText(
  obstacle: Message<"apollo.prediction.PredictionObstacle">,
  text: TextPrimitive
) {
  text.text = text.text.concat(
    `\n${Priority[obstacle.priority?.priority]} ${
      Interact[obstacle.interactive_tag.interactive_tag]
    }`
  );
}

export function convertPredictionObstacles(
  message: Message<"apollo.prediction.PredictionObstacles">
): SceneUpdate {
  var entities: SceneEntity[] = [];

  var ts = convertTimestamp(message.header.timestamp_sec);

  if (message.header.frame_id == "") message.header.frame_id = "world";
  let offset: Point3 = <Point3>{};

  if (
    message.prediction_obstacle[0]?.perception_obstacle.polygon_point !=
    undefined
  ) {
    offset =
      message.prediction_obstacle[0].perception_obstacle.polygon_point[0] ??
      <Point3>{};
  }

  for (var apollo_obj of message.prediction_obstacle) {
    // offset = get_min_coordinates(apollo_obj.perception_obstacle);

    let converted_obj = convertPerceptionObstacle(
      apollo_obj.perception_obstacle,
      ts,
      offset,
      message.header.frame_id
    );

    if (converted_obj.texts[0] != undefined)
      appendText(apollo_obj, converted_obj.texts[0]);

    converted_obj.lines.push(...convertTrajectory(apollo_obj, offset));

    entities.push(converted_obj);
  }

  return {
    deletions: [
      {
        timestamp: ts,
        type: 1,
        id: "",
      },
    ],
    entities: entities,
  };
}
