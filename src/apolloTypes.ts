
// NOTE:
// This file is generated from the current data source.
// It contains helper types for looking up message definitions by schema name or topic.
//
// You likely want to use the higher-level types in `./types` rather than the types in this file directly.

export type Message<T extends keyof MessageTypeBySchemaName> = MessageTypeBySchemaName[T];

export type Time = {
  sec: number,
  nsec: number,
};

type Duration = Time;


/**
 * MessageTypeBySchemaName enumerates the message types for for all the schema names
 * in the current data source.
 *
 * You probably want to use Message<...> instead.
 */
export type MessageTypeBySchemaName = {

  "action_msgs/GoalInfo": {
    "goal_id": MessageTypeBySchemaName["unique_identifier_msgs/UUID"],
    "stamp": Time,
  },
  "unique_identifier_msgs/UUID": {
    "uuid": Uint8Array,
  },
  "action_msgs/GoalStatus": {
    // STATUS_UNKNOWN = 0
    // STATUS_ACCEPTED = 1
    // STATUS_EXECUTING = 2
    // STATUS_CANCELING = 3
    // STATUS_SUCCEEDED = 4
    // STATUS_CANCELED = 5
    // STATUS_ABORTED = 6
    "goal_info": MessageTypeBySchemaName["action_msgs/GoalInfo"],
    "status": number,
  },
  "action_msgs/GoalStatusArray": {
    "status_list": MessageTypeBySchemaName["action_msgs/GoalStatus"][],
  },
  "actionlib_msgs/GoalID": {
    "stamp": Time,
    "id": string,
  },
  "actionlib_msgs/GoalStatus": {
    "goal_id": MessageTypeBySchemaName["actionlib_msgs/GoalID"],
    "status": number,
    // PENDING = 0
    // ACTIVE = 1
    // PREEMPTED = 2
    // SUCCEEDED = 3
    // ABORTED = 4
    // REJECTED = 5
    // PREEMPTING = 6
    // RECALLING = 7
    // RECALLED = 8
    // LOST = 9
    "text": string,
  },
  "actionlib_msgs/GoalStatusArray": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "status_list": MessageTypeBySchemaName["actionlib_msgs/GoalStatus"][],
  },
  "std_msgs/Header": {
    "seq": number,
    "stamp": Time,
    "frame_id": string,
  },
  "builtin_interfaces/Duration": {
    "sec": number,
    "nanosec": number,
  },
  "builtin_interfaces/Time": {
    "sec": number,
    "nanosec": number,
  },
  "diagnostic_msgs/DiagnosticArray": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "status": MessageTypeBySchemaName["diagnostic_msgs/DiagnosticStatus"][],
  },
  "diagnostic_msgs/DiagnosticStatus": {
    // OK = 0
    // WARN = 1
    // ERROR = 2
    // STALE = 3
    "level": number,
    "name": string,
    "message": string,
    "hardware_id": string,
    "values": MessageTypeBySchemaName["diagnostic_msgs/KeyValue"][],
  },
  "diagnostic_msgs/KeyValue": {
    "key": string,
    "value": string,
  },
  "geometry_msgs/Accel": {
    "linear": MessageTypeBySchemaName["geometry_msgs/Vector3"],
    "angular": MessageTypeBySchemaName["geometry_msgs/Vector3"],
  },
  "geometry_msgs/Vector3": {
    "x": number,
    "y": number,
    "z": number,
  },
  "geometry_msgs/AccelStamped": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "accel": MessageTypeBySchemaName["geometry_msgs/Accel"],
  },
  "geometry_msgs/AccelWithCovariance": {
    "accel": MessageTypeBySchemaName["geometry_msgs/Accel"],
    "covariance": Float64Array,
  },
  "geometry_msgs/AccelWithCovarianceStamped": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "accel": MessageTypeBySchemaName["geometry_msgs/AccelWithCovariance"],
  },
  "geometry_msgs/Inertia": {
    "m": number,
    "com": MessageTypeBySchemaName["geometry_msgs/Vector3"],
    "ixx": number,
    "ixy": number,
    "ixz": number,
    "iyy": number,
    "iyz": number,
    "izz": number,
  },
  "geometry_msgs/InertiaStamped": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "inertia": MessageTypeBySchemaName["geometry_msgs/Inertia"],
  },
  "geometry_msgs/Point": {
    "x": number,
    "y": number,
    "z": number,
  },
  "geometry_msgs/Point32": {
    "x": number,
    "y": number,
    "z": number,
  },
  "geometry_msgs/PointStamped": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "point": MessageTypeBySchemaName["geometry_msgs/Point"],
  },
  "geometry_msgs/Polygon": {
    "points": MessageTypeBySchemaName["geometry_msgs/Point32"][],
  },
  "geometry_msgs/PolygonStamped": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "polygon": MessageTypeBySchemaName["geometry_msgs/Polygon"],
  },
  "geometry_msgs/Pose": {
    "position": MessageTypeBySchemaName["geometry_msgs/Point"],
    "orientation": MessageTypeBySchemaName["geometry_msgs/Quaternion"],
  },
  "geometry_msgs/Quaternion": {
    "x": number,
    "y": number,
    "z": number,
    "w": number,
  },
  "geometry_msgs/Pose2D": {
    "x": number,
    "y": number,
    "theta": number,
  },
  "geometry_msgs/PoseArray": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "poses": MessageTypeBySchemaName["geometry_msgs/Pose"][],
  },
  "geometry_msgs/PoseStamped": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "pose": MessageTypeBySchemaName["geometry_msgs/Pose"],
  },
  "geometry_msgs/PoseWithCovariance": {
    "pose": MessageTypeBySchemaName["geometry_msgs/Pose"],
    "covariance": Float64Array,
  },
  "geometry_msgs/PoseWithCovarianceStamped": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "pose": MessageTypeBySchemaName["geometry_msgs/PoseWithCovariance"],
  },
  "geometry_msgs/QuaternionStamped": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "quaternion": MessageTypeBySchemaName["geometry_msgs/Quaternion"],
  },
  "geometry_msgs/Transform": {
    "translation": MessageTypeBySchemaName["geometry_msgs/Vector3"],
    "rotation": MessageTypeBySchemaName["geometry_msgs/Quaternion"],
  },
  "geometry_msgs/TransformStamped": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "child_frame_id": string,
    "transform": MessageTypeBySchemaName["geometry_msgs/Transform"],
  },
  "geometry_msgs/Twist": {
    "linear": MessageTypeBySchemaName["geometry_msgs/Vector3"],
    "angular": MessageTypeBySchemaName["geometry_msgs/Vector3"],
  },
  "geometry_msgs/TwistStamped": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "twist": MessageTypeBySchemaName["geometry_msgs/Twist"],
  },
  "geometry_msgs/TwistWithCovariance": {
    "twist": MessageTypeBySchemaName["geometry_msgs/Twist"],
    "covariance": Float64Array,
  },
  "geometry_msgs/TwistWithCovarianceStamped": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "twist": MessageTypeBySchemaName["geometry_msgs/TwistWithCovariance"],
  },
  "geometry_msgs/Vector3Stamped": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "vector": MessageTypeBySchemaName["geometry_msgs/Vector3"],
  },
  "geometry_msgs/Wrench": {
    "force": MessageTypeBySchemaName["geometry_msgs/Vector3"],
    "torque": MessageTypeBySchemaName["geometry_msgs/Vector3"],
  },
  "geometry_msgs/WrenchStamped": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "wrench": MessageTypeBySchemaName["geometry_msgs/Wrench"],
  },
  "lifecycle_msgs/State": {
    // PRIMARY_STATE_UNKNOWN = 0
    // PRIMARY_STATE_UNCONFIGURED = 1
    // PRIMARY_STATE_INACTIVE = 2
    // PRIMARY_STATE_ACTIVE = 3
    // PRIMARY_STATE_FINALIZED = 4
    // TRANSITION_STATE_CONFIGURING = 10
    // TRANSITION_STATE_CLEANINGUP = 11
    // TRANSITION_STATE_SHUTTINGDOWN = 12
    // TRANSITION_STATE_ACTIVATING = 13
    // TRANSITION_STATE_DEACTIVATING = 14
    // TRANSITION_STATE_ERRORPROCESSING = 15
    "id": number,
    "label": string,
  },
  "lifecycle_msgs/Transition": {
    // TRANSITION_CREATE = 0
    // TRANSITION_CONFIGURE = 1
    // TRANSITION_CLEANUP = 2
    // TRANSITION_ACTIVATE = 3
    // TRANSITION_DEACTIVATE = 4
    // TRANSITION_UNCONFIGURED_SHUTDOWN = 5
    // TRANSITION_INACTIVE_SHUTDOWN = 6
    // TRANSITION_ACTIVE_SHUTDOWN = 7
    // TRANSITION_DESTROY = 8
    // TRANSITION_ON_CONFIGURE_SUCCESS = 10
    // TRANSITION_ON_CONFIGURE_FAILURE = 11
    // TRANSITION_ON_CONFIGURE_ERROR = 12
    // TRANSITION_ON_CLEANUP_SUCCESS = 20
    // TRANSITION_ON_CLEANUP_FAILURE = 21
    // TRANSITION_ON_CLEANUP_ERROR = 22
    // TRANSITION_ON_ACTIVATE_SUCCESS = 30
    // TRANSITION_ON_ACTIVATE_FAILURE = 31
    // TRANSITION_ON_ACTIVATE_ERROR = 32
    // TRANSITION_ON_DEACTIVATE_SUCCESS = 40
    // TRANSITION_ON_DEACTIVATE_FAILURE = 41
    // TRANSITION_ON_DEACTIVATE_ERROR = 42
    // TRANSITION_ON_SHUTDOWN_SUCCESS = 50
    // TRANSITION_ON_SHUTDOWN_FAILURE = 51
    // TRANSITION_ON_SHUTDOWN_ERROR = 52
    // TRANSITION_ON_ERROR_SUCCESS = 60
    // TRANSITION_ON_ERROR_FAILURE = 61
    // TRANSITION_ON_ERROR_ERROR = 62
    // TRANSITION_CALLBACK_SUCCESS = 97
    // TRANSITION_CALLBACK_FAILURE = 98
    // TRANSITION_CALLBACK_ERROR = 99
    "id": number,
    "label": string,
  },
  "lifecycle_msgs/TransitionDescription": {
    "transition": MessageTypeBySchemaName["lifecycle_msgs/Transition"],
    "start_state": MessageTypeBySchemaName["lifecycle_msgs/State"],
    "goal_state": MessageTypeBySchemaName["lifecycle_msgs/State"],
  },
  "lifecycle_msgs/TransitionEvent": {
    "timestamp": bigint,
    "transition": MessageTypeBySchemaName["lifecycle_msgs/Transition"],
    "start_state": MessageTypeBySchemaName["lifecycle_msgs/State"],
    "goal_state": MessageTypeBySchemaName["lifecycle_msgs/State"],
  },
  "nav_msgs/GridCells": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "cell_width": number,
    "cell_height": number,
    "cells": MessageTypeBySchemaName["geometry_msgs/Point"][],
  },
  "nav_msgs/MapMetaData": {
    "map_load_time": Time,
    "resolution": number,
    "width": number,
    "height": number,
    "origin": MessageTypeBySchemaName["geometry_msgs/Pose"],
  },
  "nav_msgs/OccupancyGrid": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "info": MessageTypeBySchemaName["nav_msgs/MapMetaData"],
    "data": Int8Array,
  },
  "nav_msgs/Odometry": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "child_frame_id": string,
    "pose": MessageTypeBySchemaName["geometry_msgs/PoseWithCovariance"],
    "twist": MessageTypeBySchemaName["geometry_msgs/TwistWithCovariance"],
  },
  "nav_msgs/Path": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "poses": MessageTypeBySchemaName["geometry_msgs/PoseStamped"][],
  },
  "rcl_interfaces/FloatingPointRange": {
    "from_value": number,
    "to_value": number,
    "step": number,
  },
  "rcl_interfaces/IntegerRange": {
    "from_value": bigint,
    "to_value": bigint,
    "step": bigint,
  },
  "rcl_interfaces/ListParametersResult": {
    "names": string[],
    "prefixes": string[],
  },
  "rcl_interfaces/Log": {
    // DEBUG = 10
    // INFO = 20
    // WARN = 30
    // ERROR = 40
    // FATAL = 50
    "stamp": Time,
    "level": number,
    "name": string,
    "msg": string,
    "file": string,
    "function": string,
    "line": number,
  },
  "rcl_interfaces/Parameter": {
    "name": string,
    "value": MessageTypeBySchemaName["rcl_interfaces/ParameterValue"],
  },
  "rcl_interfaces/ParameterValue": {
    "type": number,
    "bool_value": boolean,
    "integer_value": bigint,
    "double_value": number,
    "string_value": string,
    "byte_array_value": Int8Array,
    "bool_array_value": boolean[],
    "integer_array_value": BigInt64Array,
    "double_array_value": Float64Array,
    "string_array_value": string[],
  },
  "rcl_interfaces/ParameterDescriptor": {
    "name": string,
    "type": number,
    "description": string,
    "additional_constraints": string,
    "read_only": boolean,
    "dynamic_typing": boolean,
    "floating_point_range": MessageTypeBySchemaName["rcl_interfaces/FloatingPointRange"][],
    "integer_range": MessageTypeBySchemaName["rcl_interfaces/IntegerRange"][],
  },
  "rcl_interfaces/ParameterEvent": {
    "stamp": Time,
    "node": string,
    "new_parameters": MessageTypeBySchemaName["rcl_interfaces/Parameter"][],
    "changed_parameters": MessageTypeBySchemaName["rcl_interfaces/Parameter"][],
    "deleted_parameters": MessageTypeBySchemaName["rcl_interfaces/Parameter"][],
  },
  "rcl_interfaces/ParameterEventDescriptors": {
    "new_parameters": MessageTypeBySchemaName["rcl_interfaces/ParameterDescriptor"][],
    "changed_parameters": MessageTypeBySchemaName["rcl_interfaces/ParameterDescriptor"][],
    "deleted_parameters": MessageTypeBySchemaName["rcl_interfaces/ParameterDescriptor"][],
  },
  "rcl_interfaces/ParameterType": {
    // PARAMETER_NOT_SET = 0
    // PARAMETER_BOOL = 1
    // PARAMETER_INTEGER = 2
    // PARAMETER_DOUBLE = 3
    // PARAMETER_STRING = 4
    // PARAMETER_BYTE_ARRAY = 5
    // PARAMETER_BOOL_ARRAY = 6
    // PARAMETER_INTEGER_ARRAY = 7
    // PARAMETER_DOUBLE_ARRAY = 8
    // PARAMETER_STRING_ARRAY = 9
  },
  "rcl_interfaces/SetParametersResult": {
    "successful": boolean,
    "reason": string,
  },
  "rosgraph_msgs/Clock": {
    "clock": Time,
  },
  "sensor_msgs/BatteryState": {
    // POWER_SUPPLY_STATUS_UNKNOWN = 0
    // POWER_SUPPLY_STATUS_CHARGING = 1
    // POWER_SUPPLY_STATUS_DISCHARGING = 2
    // POWER_SUPPLY_STATUS_NOT_CHARGING = 3
    // POWER_SUPPLY_STATUS_FULL = 4
    // POWER_SUPPLY_HEALTH_UNKNOWN = 0
    // POWER_SUPPLY_HEALTH_GOOD = 1
    // POWER_SUPPLY_HEALTH_OVERHEAT = 2
    // POWER_SUPPLY_HEALTH_DEAD = 3
    // POWER_SUPPLY_HEALTH_OVERVOLTAGE = 4
    // POWER_SUPPLY_HEALTH_UNSPEC_FAILURE = 5
    // POWER_SUPPLY_HEALTH_COLD = 6
    // POWER_SUPPLY_HEALTH_WATCHDOG_TIMER_EXPIRE = 7
    // POWER_SUPPLY_HEALTH_SAFETY_TIMER_EXPIRE = 8
    // POWER_SUPPLY_TECHNOLOGY_UNKNOWN = 0
    // POWER_SUPPLY_TECHNOLOGY_NIMH = 1
    // POWER_SUPPLY_TECHNOLOGY_LION = 2
    // POWER_SUPPLY_TECHNOLOGY_LIPO = 3
    // POWER_SUPPLY_TECHNOLOGY_LIFE = 4
    // POWER_SUPPLY_TECHNOLOGY_NICD = 5
    // POWER_SUPPLY_TECHNOLOGY_LIMN = 6
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "voltage": number,
    "temperature": number,
    "current": number,
    "charge": number,
    "capacity": number,
    "design_capacity": number,
    "percentage": number,
    "power_supply_status": number,
    "power_supply_health": number,
    "power_supply_technology": number,
    "present": boolean,
    "cell_voltage": Float32Array,
    "cell_temperature": Float32Array,
    "location": string,
    "serial_number": string,
  },
  "sensor_msgs/CameraInfo": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "height": number,
    "width": number,
    "distortion_model": string,
    "D": Float64Array,
    "K": Float64Array,
    "R": Float64Array,
    "P": Float64Array,
    "binning_x": number,
    "binning_y": number,
    "roi": MessageTypeBySchemaName["sensor_msgs/RegionOfInterest"],
  },
  "sensor_msgs/RegionOfInterest": {
    "x_offset": number,
    "y_offset": number,
    "height": number,
    "width": number,
    "do_rectify": boolean,
  },
  "sensor_msgs/ChannelFloat32": {
    "name": string,
    "values": Float32Array,
  },
  "sensor_msgs/CompressedImage": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "format": string,
    "data": Uint8Array,
  },
  "sensor_msgs/FluidPressure": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "fluid_pressure": number,
    "variance": number,
  },
  "sensor_msgs/Illuminance": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "illuminance": number,
    "variance": number,
  },
  "sensor_msgs/Image": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "height": number,
    "width": number,
    "encoding": string,
    "is_bigendian": number,
    "step": number,
    "data": Uint8Array,
  },
  "sensor_msgs/Imu": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "orientation": MessageTypeBySchemaName["geometry_msgs/Quaternion"],
    "orientation_covariance": Float64Array,
    "angular_velocity": MessageTypeBySchemaName["geometry_msgs/Vector3"],
    "angular_velocity_covariance": Float64Array,
    "linear_acceleration": MessageTypeBySchemaName["geometry_msgs/Vector3"],
    "linear_acceleration_covariance": Float64Array,
  },
  "sensor_msgs/JointState": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "name": string[],
    "position": Float64Array,
    "velocity": Float64Array,
    "effort": Float64Array,
  },
  "sensor_msgs/Joy": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "axes": Float32Array,
    "buttons": Int32Array,
  },
  "sensor_msgs/JoyFeedback": {
    // TYPE_LED = 0
    // TYPE_RUMBLE = 1
    // TYPE_BUZZER = 2
    "type": number,
    "id": number,
    "intensity": number,
  },
  "sensor_msgs/JoyFeedbackArray": {
    "array": MessageTypeBySchemaName["sensor_msgs/JoyFeedback"][],
  },
  "sensor_msgs/LaserEcho": {
    "echoes": Float32Array,
  },
  "sensor_msgs/LaserScan": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "angle_min": number,
    "angle_max": number,
    "angle_increment": number,
    "time_increment": number,
    "scan_time": number,
    "range_min": number,
    "range_max": number,
    "ranges": Float32Array,
    "intensities": Float32Array,
  },
  "sensor_msgs/MagneticField": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "magnetic_field": MessageTypeBySchemaName["geometry_msgs/Vector3"],
    "magnetic_field_covariance": Float64Array,
  },
  "sensor_msgs/MultiDOFJointState": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "joint_names": string[],
    "transforms": MessageTypeBySchemaName["geometry_msgs/Transform"][],
    "twist": MessageTypeBySchemaName["geometry_msgs/Twist"][],
    "wrench": MessageTypeBySchemaName["geometry_msgs/Wrench"][],
  },
  "sensor_msgs/MultiEchoLaserScan": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "angle_min": number,
    "angle_max": number,
    "angle_increment": number,
    "time_increment": number,
    "scan_time": number,
    "range_min": number,
    "range_max": number,
    "ranges": MessageTypeBySchemaName["sensor_msgs/LaserEcho"][],
    "intensities": MessageTypeBySchemaName["sensor_msgs/LaserEcho"][],
  },
  "sensor_msgs/NavSatFix": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "status": MessageTypeBySchemaName["sensor_msgs/NavSatStatus"],
    "latitude": number,
    "longitude": number,
    "altitude": number,
    "position_covariance": Float64Array,
    // COVARIANCE_TYPE_UNKNOWN = 0
    // COVARIANCE_TYPE_APPROXIMATED = 1
    // COVARIANCE_TYPE_DIAGONAL_KNOWN = 2
    // COVARIANCE_TYPE_KNOWN = 3
    "position_covariance_type": number,
  },
  "sensor_msgs/NavSatStatus": {
    // STATUS_NO_FIX = -1
    // STATUS_FIX = 0
    // STATUS_SBAS_FIX = 1
    // STATUS_GBAS_FIX = 2
    "status": number,
    // SERVICE_GPS = 1
    // SERVICE_GLONASS = 2
    // SERVICE_COMPASS = 4
    // SERVICE_GALILEO = 8
    "service": number,
  },
  "sensor_msgs/PointCloud": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "points": MessageTypeBySchemaName["geometry_msgs/Point32"][],
    "channels": MessageTypeBySchemaName["sensor_msgs/ChannelFloat32"][],
  },
  "sensor_msgs/PointCloud2": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "height": number,
    "width": number,
    "fields": MessageTypeBySchemaName["sensor_msgs/PointField"][],
    "is_bigendian": boolean,
    "point_step": number,
    "row_step": number,
    "data": Uint8Array,
    "is_dense": boolean,
  },
  "sensor_msgs/PointField": {
    // INT8 = 1
    // UINT8 = 2
    // INT16 = 3
    // UINT16 = 4
    // INT32 = 5
    // UINT32 = 6
    // FLOAT32 = 7
    // FLOAT64 = 8
    "name": string,
    "offset": number,
    "datatype": number,
    "count": number,
  },
  "sensor_msgs/Range": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    // ULTRASOUND = 0
    // INFRARED = 1
    "radiation_type": number,
    "field_of_view": number,
    "min_range": number,
    "max_range": number,
    "range": number,
  },
  "sensor_msgs/RelativeHumidity": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "relative_humidity": number,
    "variance": number,
  },
  "sensor_msgs/Temperature": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "temperature": number,
    "variance": number,
  },
  "sensor_msgs/TimeReference": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "time_ref": Time,
    "source": string,
  },
  "shape_msgs/Mesh": {
    "triangles": MessageTypeBySchemaName["shape_msgs/MeshTriangle"][],
    "vertices": MessageTypeBySchemaName["geometry_msgs/Point"][],
  },
  "shape_msgs/MeshTriangle": {
    "vertex_indices": Uint32Array,
  },
  "shape_msgs/Plane": {
    "coef": Float64Array,
  },
  "shape_msgs/SolidPrimitive": {
    // BOX = 1
    // SPHERE = 2
    // CYLINDER = 3
    // CONE = 4
    "type": number,
    "dimensions": Float64Array,
    // BOX_X = 0
    // BOX_Y = 1
    // BOX_Z = 2
    // SPHERE_RADIUS = 0
    // CYLINDER_HEIGHT = 0
    // CYLINDER_RADIUS = 1
    // CONE_HEIGHT = 0
    // CONE_RADIUS = 1
  },
  "statistics_msgs/MetricsMessage": {
    "measurement_source_name": string,
    "metrics_source": string,
    "unit": string,
    "window_start": Time,
    "window_stop": Time,
    "statistics": MessageTypeBySchemaName["statistics_msgs/StatisticDataPoint"][],
  },
  "statistics_msgs/StatisticDataPoint": {
    "data_type": number,
    "data": number,
  },
  "statistics_msgs/StatisticDataType": {
    // STATISTICS_DATA_TYPE_UNINITIALIZED = 0
    // STATISTICS_DATA_TYPE_AVERAGE = 1
    // STATISTICS_DATA_TYPE_MINIMUM = 2
    // STATISTICS_DATA_TYPE_MAXIMUM = 3
    // STATISTICS_DATA_TYPE_STDDEV = 4
    // STATISTICS_DATA_TYPE_SAMPLE_COUNT = 5
  },
  "std_msgs/Bool": {
    "data": boolean,
  },
  "std_msgs/Byte": {
    "data": number,
  },
  "std_msgs/ByteMultiArray": {
    "layout": MessageTypeBySchemaName["std_msgs/MultiArrayLayout"],
    "data": Int8Array,
  },
  "std_msgs/MultiArrayLayout": {
    "dim": MessageTypeBySchemaName["std_msgs/MultiArrayDimension"][],
    "data_offset": number,
  },
  "std_msgs/MultiArrayDimension": {
    "label": string,
    "size": number,
    "stride": number,
  },
  "std_msgs/Char": {
    "data": number,
  },
  "std_msgs/ColorRGBA": {
    "r": number,
    "g": number,
    "b": number,
    "a": number,
  },
  "std_msgs/Empty": {
  },
  "std_msgs/Float32": {
    "data": number,
  },
  "std_msgs/Float32MultiArray": {
    "layout": MessageTypeBySchemaName["std_msgs/MultiArrayLayout"],
    "data": Float32Array,
  },
  "std_msgs/Float64": {
    "data": number,
  },
  "std_msgs/Float64MultiArray": {
    "layout": MessageTypeBySchemaName["std_msgs/MultiArrayLayout"],
    "data": Float64Array,
  },
  "std_msgs/Int16": {
    "data": number,
  },
  "std_msgs/Int16MultiArray": {
    "layout": MessageTypeBySchemaName["std_msgs/MultiArrayLayout"],
    "data": Int16Array,
  },
  "std_msgs/Int32": {
    "data": number,
  },
  "std_msgs/Int32MultiArray": {
    "layout": MessageTypeBySchemaName["std_msgs/MultiArrayLayout"],
    "data": Int32Array,
  },
  "std_msgs/Int64": {
    "data": bigint,
  },
  "std_msgs/Int64MultiArray": {
    "layout": MessageTypeBySchemaName["std_msgs/MultiArrayLayout"],
    "data": BigInt64Array,
  },
  "std_msgs/Int8": {
    "data": number,
  },
  "std_msgs/Int8MultiArray": {
    "layout": MessageTypeBySchemaName["std_msgs/MultiArrayLayout"],
    "data": Int8Array,
  },
  "std_msgs/String": {
    "data": string,
  },
  "std_msgs/UInt16": {
    "data": number,
  },
  "std_msgs/UInt16MultiArray": {
    "layout": MessageTypeBySchemaName["std_msgs/MultiArrayLayout"],
    "data": Uint16Array,
  },
  "std_msgs/UInt32": {
    "data": number,
  },
  "std_msgs/UInt32MultiArray": {
    "layout": MessageTypeBySchemaName["std_msgs/MultiArrayLayout"],
    "data": Uint32Array,
  },
  "std_msgs/UInt64": {
    "data": bigint,
  },
  "std_msgs/UInt64MultiArray": {
    "layout": MessageTypeBySchemaName["std_msgs/MultiArrayLayout"],
    "data": BigUint64Array,
  },
  "std_msgs/UInt8": {
    "data": number,
  },
  "std_msgs/UInt8MultiArray": {
    "layout": MessageTypeBySchemaName["std_msgs/MultiArrayLayout"],
    "data": Uint8Array,
  },
  "stereo_msgs/DisparityImage": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "image": MessageTypeBySchemaName["sensor_msgs/Image"],
    "f": number,
    "T": number,
    "valid_window": MessageTypeBySchemaName["sensor_msgs/RegionOfInterest"],
    "min_disparity": number,
    "max_disparity": number,
    "delta_d": number,
  },
  "test_msgs/Builtins": {
    "duration_value": Duration,
    "time_value": Time,
  },
  "tf2_msgs/TF2Error": {
    // NO_ERROR = 0
    // LOOKUP_ERROR = 1
    // CONNECTIVITY_ERROR = 2
    // EXTRAPOLATION_ERROR = 3
    // INVALID_ARGUMENT_ERROR = 4
    // TIMEOUT_ERROR = 5
    // TRANSFORM_ERROR = 6
    "error": number,
    "error_string": string,
  },
  "tf2_msgs/TFMessage": {
    "transforms": MessageTypeBySchemaName["geometry_msgs/TransformStamped"][],
  },
  "trajectory_msgs/JointTrajectory": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "joint_names": string[],
    "points": MessageTypeBySchemaName["trajectory_msgs/JointTrajectoryPoint"][],
  },
  "trajectory_msgs/JointTrajectoryPoint": {
    "positions": Float64Array,
    "velocities": Float64Array,
    "accelerations": Float64Array,
    "effort": Float64Array,
    "time_from_start": Duration,
  },
  "trajectory_msgs/MultiDOFJointTrajectory": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "joint_names": string[],
    "points": MessageTypeBySchemaName["trajectory_msgs/MultiDOFJointTrajectoryPoint"][],
  },
  "trajectory_msgs/MultiDOFJointTrajectoryPoint": {
    "transforms": MessageTypeBySchemaName["geometry_msgs/Transform"][],
    "velocities": MessageTypeBySchemaName["geometry_msgs/Twist"][],
    "accelerations": MessageTypeBySchemaName["geometry_msgs/Twist"][],
    "time_from_start": Duration,
  },
  "visualization_msgs/ImageMarker": {
    // CIRCLE = 0
    // LINE_STRIP = 1
    // LINE_LIST = 2
    // POLYGON = 3
    // POINTS = 4
    // ADD = 0
    // REMOVE = 1
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "ns": string,
    "id": number,
    "type": number,
    "action": number,
    "position": MessageTypeBySchemaName["geometry_msgs/Point"],
    "scale": number,
    "outline_color": MessageTypeBySchemaName["std_msgs/ColorRGBA"],
    "filled": number,
    "fill_color": MessageTypeBySchemaName["std_msgs/ColorRGBA"],
    "lifetime": Duration,
    "points": MessageTypeBySchemaName["geometry_msgs/Point"][],
    "outline_colors": MessageTypeBySchemaName["std_msgs/ColorRGBA"][],
  },
  "visualization_msgs/InteractiveMarker": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "pose": MessageTypeBySchemaName["geometry_msgs/Pose"],
    "name": string,
    "description": string,
    "scale": number,
    "menu_entries": MessageTypeBySchemaName["visualization_msgs/MenuEntry"][],
    "controls": MessageTypeBySchemaName["visualization_msgs/InteractiveMarkerControl"][],
  },
  "visualization_msgs/MenuEntry": {
    "id": number,
    "parent_id": number,
    "title": string,
    "command": string,
    // FEEDBACK = 0
    // ROSRUN = 1
    // ROSLAUNCH = 2
    "command_type": number,
  },
  "visualization_msgs/InteractiveMarkerControl": {
    "name": string,
    "orientation": MessageTypeBySchemaName["geometry_msgs/Quaternion"],
    // INHERIT = 0
    // FIXED = 1
    // VIEW_FACING = 2
    "orientation_mode": number,
    // NONE = 0
    // MENU = 1
    // BUTTON = 2
    // MOVE_AXIS = 3
    // MOVE_PLANE = 4
    // ROTATE_AXIS = 5
    // MOVE_ROTATE = 6
    // MOVE_3D = 7
    // ROTATE_3D = 8
    // MOVE_ROTATE_3D = 9
    "interaction_mode": number,
    "always_visible": boolean,
    "markers": MessageTypeBySchemaName["visualization_msgs/Marker"][],
    "independent_marker_orientation": boolean,
    "description": string,
  },
  "visualization_msgs/Marker": {
    // ARROW = 0
    // CUBE = 1
    // SPHERE = 2
    // CYLINDER = 3
    // LINE_STRIP = 4
    // LINE_LIST = 5
    // CUBE_LIST = 6
    // SPHERE_LIST = 7
    // POINTS = 8
    // TEXT_VIEW_FACING = 9
    // MESH_RESOURCE = 10
    // TRIANGLE_LIST = 11
    // ADD = 0
    // MODIFY = 0
    // DELETE = 2
    // DELETEALL = 3
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "ns": string,
    "id": number,
    "type": number,
    "action": number,
    "pose": MessageTypeBySchemaName["geometry_msgs/Pose"],
    "scale": MessageTypeBySchemaName["geometry_msgs/Vector3"],
    "color": MessageTypeBySchemaName["std_msgs/ColorRGBA"],
    "lifetime": Duration,
    "frame_locked": boolean,
    "points": MessageTypeBySchemaName["geometry_msgs/Point"][],
    "colors": MessageTypeBySchemaName["std_msgs/ColorRGBA"][],
    "text": string,
    "mesh_resource": string,
    "mesh_use_embedded_materials": boolean,
  },
  "visualization_msgs/InteractiveMarkerFeedback": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "client_id": string,
    "marker_name": string,
    "control_name": string,
    // KEEP_ALIVE = 0
    // POSE_UPDATE = 1
    // MENU_SELECT = 2
    // BUTTON_CLICK = 3
    // MOUSE_DOWN = 4
    // MOUSE_UP = 5
    "event_type": number,
    "pose": MessageTypeBySchemaName["geometry_msgs/Pose"],
    "menu_entry_id": number,
    "mouse_point": MessageTypeBySchemaName["geometry_msgs/Point"],
    "mouse_point_valid": boolean,
  },
  "visualization_msgs/InteractiveMarkerInit": {
    "server_id": string,
    "seq_num": bigint,
    "markers": MessageTypeBySchemaName["visualization_msgs/InteractiveMarker"][],
  },
  "visualization_msgs/InteractiveMarkerPose": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "pose": MessageTypeBySchemaName["geometry_msgs/Pose"],
    "name": string,
  },
  "visualization_msgs/InteractiveMarkerUpdate": {
    "server_id": string,
    "seq_num": bigint,
    // KEEP_ALIVE = 0
    // UPDATE = 1
    "type": number,
    "markers": MessageTypeBySchemaName["visualization_msgs/InteractiveMarker"][],
    "poses": MessageTypeBySchemaName["visualization_msgs/InteractiveMarkerPose"][],
    "erases": string[],
  },
  "visualization_msgs/MarkerArray": {
    "markers": MessageTypeBySchemaName["visualization_msgs/Marker"][],
  },
  "rosgraph_msgs/Log": {
    // DEBUG = 1
    // INFO = 2
    // WARN = 4
    // ERROR = 8
    // FATAL = 16
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "level": number,
    "name": string,
    "msg": string,
    "file": string,
    "function": string,
    "line": number,
    "topics": string[],
  },
  "rosgraph_msgs/TopicStatistics": {
    "topic": string,
    "node_pub": string,
    "node_sub": string,
    "window_start": Time,
    "window_stop": Time,
    "delivered_msgs": number,
    "dropped_msgs": number,
    "traffic": number,
    "period_mean": Duration,
    "period_stddev": Duration,
    "period_max": Duration,
    "stamp_age_mean": Duration,
    "stamp_age_stddev": Duration,
    "stamp_age_max": Duration,
  },
  "std_msgs/Duration": {
    "data": Duration,
  },
  "std_msgs/Time": {
    "data": Time,
  },
  "velodyne_msgs/VelodynePacket": {
    "stamp": Time,
    "data": Uint8Array,
  },
  "velodyne_msgs/VelodyneScan": {
    "header": MessageTypeBySchemaName["std_msgs/Header"],
    "packets": MessageTypeBySchemaName["velodyne_msgs/VelodynePacket"][],
  },
  "foxglove_msgs/ArrowPrimitive": {
    "pose": MessageTypeBySchemaName["geometry_msgs/Pose"],
    "shaft_length": number,
    "shaft_diameter": number,
    "head_length": number,
    "head_diameter": number,
    "color": MessageTypeBySchemaName["foxglove_msgs/Color"],
  },
  "foxglove.ArrowPrimitive": {
    "pose": MessageTypeBySchemaName["geometry_msgs/Pose"],
    "shaft_length": number,
    "shaft_diameter": number,
    "head_length": number,
    "head_diameter": number,
    "color": MessageTypeBySchemaName["foxglove_msgs/Color"],
  },
  "foxglove_msgs/CameraCalibration": {
    "timestamp": Time,
    "frame_id": string,
    "width": number,
    "height": number,
    "distortion_model": string,
    "D": Float64Array,
    "K": Float64Array,
    "R": Float64Array,
    "P": Float64Array,
  },
  "foxglove.CameraCalibration": {
    "timestamp": Time,
    "frame_id": string,
    "width": number,
    "height": number,
    "distortion_model": string,
    "D": Float64Array,
    "K": Float64Array,
    "R": Float64Array,
    "P": Float64Array,
  },
  "foxglove_msgs/CircleAnnotation": {
    "timestamp": Time,
    "position": MessageTypeBySchemaName["foxglove_msgs/Point2"],
    "diameter": number,
    "thickness": number,
    "fill_color": MessageTypeBySchemaName["foxglove_msgs/Color"],
    "outline_color": MessageTypeBySchemaName["foxglove_msgs/Color"],
  },
  "foxglove.CircleAnnotation": {
    "timestamp": Time,
    "position": MessageTypeBySchemaName["foxglove_msgs/Point2"],
    "diameter": number,
    "thickness": number,
    "fill_color": MessageTypeBySchemaName["foxglove_msgs/Color"],
    "outline_color": MessageTypeBySchemaName["foxglove_msgs/Color"],
  },
  "foxglove_msgs/Color": {
    "r": number,
    "g": number,
    "b": number,
    "a": number,
  },
  "foxglove.Color": {
    "r": number,
    "g": number,
    "b": number,
    "a": number,
  },
  "foxglove_msgs/CompressedImage": {
    "timestamp": Time,
    "frame_id": string,
    "data": Uint8Array,
    "format": string,
  },
  "foxglove.CompressedImage": {
    "timestamp": MessageTypeBySchemaName["google.protobuf.Timestamp"],
    "frame_id": string,
    "data": Uint8Array,
    "format": string,
  },
  "foxglove_msgs/CylinderPrimitive": {
    "pose": MessageTypeBySchemaName["geometry_msgs/Pose"],
    "size": MessageTypeBySchemaName["geometry_msgs/Vector3"],
    "bottom_scale": number,
    "top_scale": number,
    "color": MessageTypeBySchemaName["foxglove_msgs/Color"],
  },
  "foxglove.CylinderPrimitive": {
    "pose": MessageTypeBySchemaName["geometry_msgs/Pose"],
    "size": MessageTypeBySchemaName["geometry_msgs/Vector3"],
    "bottom_scale": number,
    "top_scale": number,
    "color": MessageTypeBySchemaName["foxglove_msgs/Color"],
  },
  "foxglove_msgs/CubePrimitive": {
    "pose": MessageTypeBySchemaName["geometry_msgs/Pose"],
    "size": MessageTypeBySchemaName["geometry_msgs/Vector3"],
    "color": MessageTypeBySchemaName["foxglove_msgs/Color"],
  },
  "foxglove.CubePrimitive": {
    "pose": MessageTypeBySchemaName["geometry_msgs/Pose"],
    "size": MessageTypeBySchemaName["geometry_msgs/Vector3"],
    "color": MessageTypeBySchemaName["foxglove_msgs/Color"],
  },
  "foxglove_msgs/FrameTransform": {
    "timestamp": Time,
    "parent_frame_id": string,
    "child_frame_id": string,
    "translation": MessageTypeBySchemaName["geometry_msgs/Vector3"],
    "rotation": MessageTypeBySchemaName["geometry_msgs/Quaternion"],
  },
  "foxglove.FrameTransform": {
    "timestamp": MessageTypeBySchemaName["google.protobuf.Timestamp"],
    "parent_frame_id": string,
    "child_frame_id": string,
    "translation": MessageTypeBySchemaName["foxglove.Vector3"],
    "rotation": MessageTypeBySchemaName["foxglove.Quaternion"],
  },
  "foxglove_msgs/FrameTransforms": {
    "transforms": MessageTypeBySchemaName["foxglove_msgs/FrameTransform"][],
  },
  "foxglove.FrameTransforms": {
    "transforms": MessageTypeBySchemaName["foxglove_msgs/FrameTransform"][],
  },
  "foxglove_msgs/GeoJSON": {
    "geojson": string,
  },
  "foxglove.GeoJSON": {
    "geojson": string,
  },
  "foxglove_msgs/Grid": {
    "timestamp": Time,
    "frame_id": string,
    "pose": MessageTypeBySchemaName["geometry_msgs/Pose"],
    "column_count": number,
    "cell_size": MessageTypeBySchemaName["foxglove_msgs/Vector2"],
    "row_stride": number,
    "cell_stride": number,
    "fields": MessageTypeBySchemaName["foxglove_msgs/PackedElementField"][],
    "data": Uint8Array,
  },
  "foxglove.Grid": {
    "timestamp": Time,
    "frame_id": string,
    "pose": MessageTypeBySchemaName["geometry_msgs/Pose"],
    "column_count": number,
    "cell_size": MessageTypeBySchemaName["foxglove_msgs/Vector2"],
    "row_stride": number,
    "cell_stride": number,
    "fields": MessageTypeBySchemaName["foxglove_msgs/PackedElementField"][],
    "data": Uint8Array,
  },
  "foxglove_msgs/ImageAnnotations": {
    "circles": MessageTypeBySchemaName["foxglove_msgs/CircleAnnotation"][],
    "points": MessageTypeBySchemaName["foxglove_msgs/PointsAnnotation"][],
    "texts": MessageTypeBySchemaName["foxglove_msgs/TextAnnotation"][],
  },
  "foxglove.ImageAnnotations": {
    "circles": MessageTypeBySchemaName["foxglove_msgs/CircleAnnotation"][],
    "points": MessageTypeBySchemaName["foxglove_msgs/PointsAnnotation"][],
    "texts": MessageTypeBySchemaName["foxglove_msgs/TextAnnotation"][],
  },
  "foxglove_msgs/KeyValuePair": {
    "key": string,
    "value": string,
  },
  "foxglove.KeyValuePair": {
    "key": string,
    "value": string,
  },
  "foxglove_msgs/LaserScan": {
    "timestamp": Time,
    "frame_id": string,
    "pose": MessageTypeBySchemaName["geometry_msgs/Pose"],
    "start_angle": number,
    "end_angle": number,
    "ranges": Float64Array,
    "intensities": Float64Array,
  },
  "foxglove.LaserScan": {
    "timestamp": Time,
    "frame_id": string,
    "pose": MessageTypeBySchemaName["geometry_msgs/Pose"],
    "start_angle": number,
    "end_angle": number,
    "ranges": Float64Array,
    "intensities": Float64Array,
  },
  "foxglove_msgs/LinePrimitive": {
    // LINE_STRIP = 0
    // LINE_LOOP = 1
    // LINE_LIST = 2
    "type": number,
    "pose": MessageTypeBySchemaName["geometry_msgs/Pose"],
    "thickness": number,
    "scale_invariant": boolean,
    "points": MessageTypeBySchemaName["geometry_msgs/Point"][],
    "color": MessageTypeBySchemaName["foxglove_msgs/Color"],
    "colors": MessageTypeBySchemaName["foxglove_msgs/Color"][],
    "indices": Uint32Array,
  },
  "foxglove.LinePrimitive": {
    // LINE_STRIP = 0
    // LINE_LOOP = 1
    // LINE_LIST = 2
    "type": number,
    "pose": MessageTypeBySchemaName["geometry_msgs/Pose"],
    "thickness": number,
    "scale_invariant": boolean,
    "points": MessageTypeBySchemaName["geometry_msgs/Point"][],
    "color": MessageTypeBySchemaName["foxglove_msgs/Color"],
    "colors": MessageTypeBySchemaName["foxglove_msgs/Color"][],
    "indices": Uint32Array,
  },
  "foxglove_msgs/LocationFix": {
    "timestamp": Time,
    "frame_id": string,
    "latitude": number,
    "longitude": number,
    "altitude": number,
    "position_covariance": Float64Array,
    // UNKNOWN = 0
    // APPROXIMATED = 1
    // DIAGONAL_KNOWN = 2
    // KNOWN = 3
    "position_covariance_type": number,
  },
  "foxglove.LocationFix": {
    "timestamp": Time,
    "frame_id": string,
    "latitude": number,
    "longitude": number,
    "altitude": number,
    "position_covariance": Float64Array,
    // UNKNOWN = 0
    // APPROXIMATED = 1
    // DIAGONAL_KNOWN = 2
    // KNOWN = 3
    "position_covariance_type": number,
  },
  "foxglove_msgs/Log": {
    "timestamp": Time,
    // UNKNOWN = 0
    // DEBUG = 1
    // INFO = 2
    // WARNING = 3
    // ERROR = 4
    // FATAL = 5
    "level": number,
    "message": string,
    "name": string,
    "file": string,
    "line": number,
  },
  "foxglove.Log": {
    "timestamp": Time,
    // UNKNOWN = 0
    // DEBUG = 1
    // INFO = 2
    // WARNING = 3
    // ERROR = 4
    // FATAL = 5
    "level": number,
    "message": string,
    "name": string,
    "file": string,
    "line": number,
  },
  "foxglove_msgs/SceneEntityDeletion": {
    "timestamp": Time,
    // MATCHING_ID = 0
    // ALL = 1
    "type": number,
    "id": string,
  },
  "foxglove.SceneEntityDeletion": {
    "timestamp": Time,
    // MATCHING_ID = 0
    // ALL = 1
    "type": number,
    "id": string,
  },
  "foxglove_msgs/SceneEntity": {
    "timestamp": Time,
    "frame_id": string,
    "id": string,
    "lifetime": Duration,
    "frame_locked": boolean,
    "metadata": MessageTypeBySchemaName["foxglove_msgs/KeyValuePair"][],
    "arrows": MessageTypeBySchemaName["foxglove_msgs/ArrowPrimitive"][],
    "cubes": MessageTypeBySchemaName["foxglove_msgs/CubePrimitive"][],
    "spheres": MessageTypeBySchemaName["foxglove_msgs/SpherePrimitive"][],
    "cylinders": MessageTypeBySchemaName["foxglove_msgs/CylinderPrimitive"][],
    "lines": MessageTypeBySchemaName["foxglove_msgs/LinePrimitive"][],
    "triangles": MessageTypeBySchemaName["foxglove_msgs/TriangleListPrimitive"][],
    "texts": MessageTypeBySchemaName["foxglove_msgs/TextPrimitive"][],
    "models": MessageTypeBySchemaName["foxglove_msgs/ModelPrimitive"][],
  },
  "foxglove.SceneEntity": {
    "timestamp": Time,
    "frame_id": string,
    "id": string,
    "lifetime": Duration,
    "frame_locked": boolean,
    "metadata": MessageTypeBySchemaName["foxglove_msgs/KeyValuePair"][],
    "arrows": MessageTypeBySchemaName["foxglove_msgs/ArrowPrimitive"][],
    "cubes": MessageTypeBySchemaName["foxglove_msgs/CubePrimitive"][],
    "spheres": MessageTypeBySchemaName["foxglove_msgs/SpherePrimitive"][],
    "cylinders": MessageTypeBySchemaName["foxglove_msgs/CylinderPrimitive"][],
    "lines": MessageTypeBySchemaName["foxglove_msgs/LinePrimitive"][],
    "triangles": MessageTypeBySchemaName["foxglove_msgs/TriangleListPrimitive"][],
    "texts": MessageTypeBySchemaName["foxglove_msgs/TextPrimitive"][],
    "models": MessageTypeBySchemaName["foxglove_msgs/ModelPrimitive"][],
  },
  "foxglove_msgs/SceneUpdate": {
    "deletions": MessageTypeBySchemaName["foxglove_msgs/SceneEntityDeletion"][],
    "entities": MessageTypeBySchemaName["foxglove_msgs/SceneEntity"][],
  },
  "foxglove.SceneUpdate": {
    "deletions": MessageTypeBySchemaName["foxglove_msgs/SceneEntityDeletion"][],
    "entities": MessageTypeBySchemaName["foxglove_msgs/SceneEntity"][],
  },
  "foxglove_msgs/ModelPrimitive": {
    "pose": MessageTypeBySchemaName["geometry_msgs/Pose"],
    "scale": MessageTypeBySchemaName["geometry_msgs/Vector3"],
    "color": MessageTypeBySchemaName["foxglove_msgs/Color"],
    "override_color": boolean,
    "url": string,
    "media_type": string,
    "data": Uint8Array,
  },
  "foxglove.ModelPrimitive": {
    "pose": MessageTypeBySchemaName["geometry_msgs/Pose"],
    "scale": MessageTypeBySchemaName["geometry_msgs/Vector3"],
    "color": MessageTypeBySchemaName["foxglove_msgs/Color"],
    "override_color": boolean,
    "url": string,
    "media_type": string,
    "data": Uint8Array,
  },
  "foxglove_msgs/PackedElementField": {
    "name": string,
    "offset": number,
    // UNKNOWN = 0
    // UINT8 = 1
    // INT8 = 2
    // UINT16 = 3
    // INT16 = 4
    // UINT32 = 5
    // INT32 = 6
    // FLOAT32 = 7
    // FLOAT64 = 8
    "type": number,
  },
  "foxglove.PackedElementField": {
    "name": string,
    "offset": number,
    // UNKNOWN = 0
    // UINT8 = 1
    // INT8 = 2
    // UINT16 = 3
    // INT16 = 4
    // UINT32 = 5
    // INT32 = 6
    // FLOAT32 = 7
    // FLOAT64 = 8
    "type": number,
  },
  "foxglove_msgs/Point2": {
    "x": number,
    "y": number,
  },
  "foxglove.Point2": {
    "x": number,
    "y": number,
  },
  "foxglove_msgs/Point3": {
    "x": number,
    "y": number,
    "z": number,
  },
  "foxglove.Point3": {
    "x": number,
    "y": number,
    "z": number,
  },
  "foxglove_msgs/PointCloud": {
    "timestamp": Time,
    "frame_id": string,
    "pose": MessageTypeBySchemaName["geometry_msgs/Pose"],
    "point_stride": number,
    "fields": MessageTypeBySchemaName["foxglove_msgs/PackedElementField"][],
    "data": Uint8Array,
  },
  "foxglove.PointCloud": {
    "timestamp": Time,
    "frame_id": string,
    "pose": MessageTypeBySchemaName["geometry_msgs/Pose"],
    "point_stride": number,
    "fields": MessageTypeBySchemaName["foxglove_msgs/PackedElementField"][],
    "data": Uint8Array,
  },
  "foxglove_msgs/PointsAnnotation": {
    "timestamp": Time,
    // UNKNOWN = 0
    // POINTS = 1
    // LINE_LOOP = 2
    // LINE_STRIP = 3
    // LINE_LIST = 4
    "type": number,
    "points": MessageTypeBySchemaName["foxglove_msgs/Point2"][],
    "outline_color": MessageTypeBySchemaName["foxglove_msgs/Color"],
    "outline_colors": MessageTypeBySchemaName["foxglove_msgs/Color"][],
    "fill_color": MessageTypeBySchemaName["foxglove_msgs/Color"],
    "thickness": number,
  },
  "foxglove.PointsAnnotation": {
    "timestamp": Time,
    // UNKNOWN = 0
    // POINTS = 1
    // LINE_LOOP = 2
    // LINE_STRIP = 3
    // LINE_LIST = 4
    "type": number,
    "points": MessageTypeBySchemaName["foxglove_msgs/Point2"][],
    "outline_color": MessageTypeBySchemaName["foxglove_msgs/Color"],
    "outline_colors": MessageTypeBySchemaName["foxglove_msgs/Color"][],
    "fill_color": MessageTypeBySchemaName["foxglove_msgs/Color"],
    "thickness": number,
  },
  "foxglove_msgs/Pose": {
    "position": MessageTypeBySchemaName["geometry_msgs/Vector3"],
    "orientation": MessageTypeBySchemaName["geometry_msgs/Quaternion"],
  },
  "foxglove.Pose": {
    "position": MessageTypeBySchemaName["geometry_msgs/Vector3"],
    "orientation": MessageTypeBySchemaName["geometry_msgs/Quaternion"],
  },
  "foxglove_msgs/PoseInFrame": {
    "timestamp": Time,
    "frame_id": string,
    "pose": MessageTypeBySchemaName["geometry_msgs/Pose"],
  },
  "foxglove.PoseInFrame": {
    "timestamp": Time,
    "frame_id": string,
    "pose": MessageTypeBySchemaName["geometry_msgs/Pose"],
  },
  "foxglove_msgs/PosesInFrame": {
    "timestamp": Time,
    "frame_id": string,
    "poses": MessageTypeBySchemaName["geometry_msgs/Pose"][],
  },
  "foxglove.PosesInFrame": {
    "timestamp": Time,
    "frame_id": string,
    "poses": MessageTypeBySchemaName["geometry_msgs/Pose"][],
  },
  "foxglove_msgs/Quaternion": {
    "x": number,
    "y": number,
    "z": number,
    "w": number,
  },
  "foxglove.Quaternion": {
    "x": number,
    "y": number,
    "z": number,
    "w": number,
  },
  "foxglove_msgs/RawImage": {
    "timestamp": Time,
    "frame_id": string,
    "width": number,
    "height": number,
    "encoding": string,
    "step": number,
    "data": Uint8Array,
  },
  "foxglove.RawImage": {
    "timestamp": Time,
    "frame_id": string,
    "width": number,
    "height": number,
    "encoding": string,
    "step": number,
    "data": Uint8Array,
  },
  "foxglove_msgs/SpherePrimitive": {
    "pose": MessageTypeBySchemaName["geometry_msgs/Pose"],
    "size": MessageTypeBySchemaName["geometry_msgs/Vector3"],
    "color": MessageTypeBySchemaName["foxglove_msgs/Color"],
  },
  "foxglove.SpherePrimitive": {
    "pose": MessageTypeBySchemaName["geometry_msgs/Pose"],
    "size": MessageTypeBySchemaName["geometry_msgs/Vector3"],
    "color": MessageTypeBySchemaName["foxglove_msgs/Color"],
  },
  "foxglove_msgs/TextAnnotation": {
    "timestamp": Time,
    "position": MessageTypeBySchemaName["foxglove_msgs/Point2"],
    "text": string,
    "font_size": number,
    "text_color": MessageTypeBySchemaName["foxglove_msgs/Color"],
    "background_color": MessageTypeBySchemaName["foxglove_msgs/Color"],
  },
  "foxglove.TextAnnotation": {
    "timestamp": Time,
    "position": MessageTypeBySchemaName["foxglove_msgs/Point2"],
    "text": string,
    "font_size": number,
    "text_color": MessageTypeBySchemaName["foxglove_msgs/Color"],
    "background_color": MessageTypeBySchemaName["foxglove_msgs/Color"],
  },
  "foxglove_msgs/TextPrimitive": {
    "pose": MessageTypeBySchemaName["geometry_msgs/Pose"],
    "billboard": boolean,
    "font_size": number,
    "scale_invariant": boolean,
    "color": MessageTypeBySchemaName["foxglove_msgs/Color"],
    "text": string,
  },
  "foxglove.TextPrimitive": {
    "pose": MessageTypeBySchemaName["geometry_msgs/Pose"],
    "billboard": boolean,
    "font_size": number,
    "scale_invariant": boolean,
    "color": MessageTypeBySchemaName["foxglove_msgs/Color"],
    "text": string,
  },
  "foxglove_msgs/TriangleListPrimitive": {
    "pose": MessageTypeBySchemaName["geometry_msgs/Pose"],
    "points": MessageTypeBySchemaName["geometry_msgs/Point"][],
    "color": MessageTypeBySchemaName["foxglove_msgs/Color"],
    "colors": MessageTypeBySchemaName["foxglove_msgs/Color"][],
    "indices": Uint32Array,
  },
  "foxglove.TriangleListPrimitive": {
    "pose": MessageTypeBySchemaName["geometry_msgs/Pose"],
    "points": MessageTypeBySchemaName["geometry_msgs/Point"][],
    "color": MessageTypeBySchemaName["foxglove_msgs/Color"],
    "colors": MessageTypeBySchemaName["foxglove_msgs/Color"][],
    "indices": Uint32Array,
  },
  "foxglove_msgs/Vector2": {
    "x": number,
    "y": number,
  },
  "foxglove.Vector2": {
    "x": number,
    "y": number,
  },
  "foxglove_msgs/Vector3": {
    "x": number,
    "y": number,
    "z": number,
  },
  "foxglove.Vector3": {
    "x": number,
    "y": number,
    "z": number,
  },
  "foxglove_msgs/ImageMarkerArray": {
    "markers": MessageTypeBySchemaName["visualization_msgs/ImageMarker"][],
  },
  "apollo.canbus.Chassis": {
    "engine_started": boolean,
    "engine_rpm": number,
    "speed_mps": number,
    "odometer_m": number,
    "fuel_range_m": number,
    "throttle_percentage": number,
    "brake_percentage": number,
    "steering_percentage": number,
    "steering_torque_nm": number,
    "parking_brake": boolean,
    "high_beam_signal": boolean,
    "low_beam_signal": boolean,
    "left_turn_signal": boolean,
    "right_turn_signal": boolean,
    "horn": boolean,
    "wiper": boolean,
    "disengage_status": boolean,
    // COMPLETE_MANUAL = undefined
    // COMPLETE_AUTO_DRIVE = undefined
    // AUTO_STEER_ONLY = undefined
    // AUTO_SPEED_ONLY = undefined
    // EMERGENCY_MODE = undefined
    "driving_mode": number,
    // NO_ERROR = undefined
    // CMD_NOT_IN_PERIOD = undefined
    // CHASSIS_ERROR = undefined
    // CHASSIS_ERROR_ON_STEER = undefined
    // CHASSIS_ERROR_ON_BRAKE = undefined
    // CHASSIS_ERROR_ON_THROTTLE = undefined
    // CHASSIS_ERROR_ON_GEAR = undefined
    // MANUAL_INTERVENTION = undefined
    // CHASSIS_CAN_NOT_IN_PERIOD = undefined
    // UNKNOWN_ERROR = undefined
    "error_code": number,
    // GEAR_NEUTRAL = undefined
    // GEAR_DRIVE = undefined
    // GEAR_REVERSE = undefined
    // GEAR_PARKING = undefined
    // GEAR_LOW = undefined
    // GEAR_INVALID = undefined
    // GEAR_NONE = undefined
    "gear_location": number,
    "steering_timestamp": number,
    "header": MessageTypeBySchemaName["apollo.common.Header"],
    "chassis_error_mask": number,
    "signal": MessageTypeBySchemaName["apollo.common.VehicleSignal"],
    "chassis_gps": MessageTypeBySchemaName["apollo.canbus.ChassisGPS"],
    "engage_advice": MessageTypeBySchemaName["apollo.common.EngageAdvice"],
    "wheel_speed": MessageTypeBySchemaName["apollo.canbus.WheelSpeed"],
    "surround": MessageTypeBySchemaName["apollo.canbus.Surround"],
    "license": MessageTypeBySchemaName["apollo.canbus.License"],
    "vehicle_id": MessageTypeBySchemaName["apollo.common.VehicleID"],
    "battery_soc_percentage": number,
    "throttle_percentage_cmd": number,
    "brake_percentage_cmd": number,
    "steering_percentage_cmd": number,
    // BUMPER_INVALID = undefined
    // BUMPER_NORMAL = undefined
    // BUMPER_PRESSED = undefined
    "front_bumper_event": number,
    // BUMPER_INVALID = undefined
    // BUMPER_NORMAL = undefined
    // BUMPER_PRESSED = undefined
    "back_bumper_event": number,
    "check_response": MessageTypeBySchemaName["apollo.canbus.CheckResponse"],
  },
  "apollo.common.Header": {
    "timestamp_sec": number,
    "module_name": string,
    "sequence_num": number,
    "lidar_timestamp": bigint,
    "camera_timestamp": bigint,
    "radar_timestamp": bigint,
    "version": number,
    "status": MessageTypeBySchemaName["apollo.common.StatusPb"],
    "frame_id": string,
  },
  "apollo.common.StatusPb": {
    // OK = undefined
    // CONTROL_ERROR = undefined
    // CONTROL_INIT_ERROR = undefined
    // CONTROL_COMPUTE_ERROR = undefined
    // CONTROL_ESTOP_ERROR = undefined
    // PERFECT_CONTROL_ERROR = undefined
    // CANBUS_ERROR = undefined
    // CAN_CLIENT_ERROR_BASE = undefined
    // CAN_CLIENT_ERROR_OPEN_DEVICE_FAILED = undefined
    // CAN_CLIENT_ERROR_FRAME_NUM = undefined
    // CAN_CLIENT_ERROR_SEND_FAILED = undefined
    // CAN_CLIENT_ERROR_RECV_FAILED = undefined
    // LOCALIZATION_ERROR = undefined
    // LOCALIZATION_ERROR_MSG = undefined
    // LOCALIZATION_ERROR_LIDAR = undefined
    // LOCALIZATION_ERROR_INTEG = undefined
    // LOCALIZATION_ERROR_GNSS = undefined
    // PERCEPTION_ERROR = undefined
    // PERCEPTION_ERROR_TF = undefined
    // PERCEPTION_ERROR_PROCESS = undefined
    // PERCEPTION_FATAL = undefined
    // PERCEPTION_ERROR_NONE = undefined
    // PERCEPTION_ERROR_UNKNOWN = undefined
    // PREDICTION_ERROR = undefined
    // PLANNING_ERROR = undefined
    // PLANNING_ERROR_NOT_READY = undefined
    // HDMAP_DATA_ERROR = undefined
    // ROUTING_ERROR = undefined
    // ROUTING_ERROR_REQUEST = undefined
    // ROUTING_ERROR_RESPONSE = undefined
    // ROUTING_ERROR_NOT_READY = undefined
    // END_OF_INPUT = undefined
    // HTTP_LOGIC_ERROR = undefined
    // HTTP_RUNTIME_ERROR = undefined
    // RELATIVE_MAP_ERROR = undefined
    // RELATIVE_MAP_NOT_READY = undefined
    // DRIVER_ERROR_GNSS = undefined
    // DRIVER_ERROR_VELODYNE = undefined
    // STORYTELLING_ERROR = undefined
    "error_code": number,
    "msg": string,
  },
  "apollo.common.VehicleSignal": {
    // TURN_NONE = undefined
    // TURN_LEFT = undefined
    // TURN_RIGHT = undefined
    // TURN_HAZARD_WARNING = undefined
    "turn_signal": number,
    "high_beam": boolean,
    "low_beam": boolean,
    "horn": boolean,
    "emergency_light": boolean,
  },
  "apollo.canbus.ChassisGPS": {
    "latitude": number,
    "longitude": number,
    "gps_valid": boolean,
    "year": number,
    "month": number,
    "day": number,
    "hours": number,
    "minutes": number,
    "seconds": number,
    "compass_direction": number,
    "pdop": number,
    "is_gps_fault": boolean,
    "is_inferred": boolean,
    "altitude": number,
    "heading": number,
    "hdop": number,
    "vdop": number,
    // FIX_NO = undefined
    // FIX_2D = undefined
    // FIX_3D = undefined
    // FIX_INVALID = undefined
    "quality": number,
    "num_satellites": number,
    "gps_speed": number,
  },
  "apollo.common.EngageAdvice": {
    // UNKNOWN = undefined
    // DISALLOW_ENGAGE = undefined
    // READY_TO_ENGAGE = undefined
    // KEEP_ENGAGED = undefined
    // PREPARE_DISENGAGE = undefined
    "advice": number,
    "reason": string,
  },
  "apollo.canbus.WheelSpeed": {
    "is_wheel_spd_rr_valid": boolean,
    // FORWARD = undefined
    // BACKWARD = undefined
    // STANDSTILL = undefined
    // INVALID = undefined
    "wheel_direction_rr": number,
    "wheel_spd_rr": number,
    "is_wheel_spd_rl_valid": boolean,
    // FORWARD = undefined
    // BACKWARD = undefined
    // STANDSTILL = undefined
    // INVALID = undefined
    "wheel_direction_rl": number,
    "wheel_spd_rl": number,
    "is_wheel_spd_fr_valid": boolean,
    // FORWARD = undefined
    // BACKWARD = undefined
    // STANDSTILL = undefined
    // INVALID = undefined
    "wheel_direction_fr": number,
    "wheel_spd_fr": number,
    "is_wheel_spd_fl_valid": boolean,
    // FORWARD = undefined
    // BACKWARD = undefined
    // STANDSTILL = undefined
    // INVALID = undefined
    "wheel_direction_fl": number,
    "wheel_spd_fl": number,
  },
  "apollo.canbus.Surround": {
    "cross_traffic_alert_left": boolean,
    "cross_traffic_alert_left_enabled": boolean,
    "blind_spot_left_alert": boolean,
    "blind_spot_left_alert_enabled": boolean,
    "cross_traffic_alert_right": boolean,
    "cross_traffic_alert_right_enabled": boolean,
    "blind_spot_right_alert": boolean,
    "blind_spot_right_alert_enabled": boolean,
    "sonar00": number,
    "sonar01": number,
    "sonar02": number,
    "sonar03": number,
    "sonar04": number,
    "sonar05": number,
    "sonar06": number,
    "sonar07": number,
    "sonar08": number,
    "sonar09": number,
    "sonar10": number,
    "sonar11": number,
    "sonar_enabled": boolean,
    "sonar_fault": boolean,
    "sonar_range": Float64Array,
    "sonar": MessageTypeBySchemaName["apollo.canbus.Sonar"][],
  },
  "apollo.canbus.Sonar": {
    "range": number,
    "translation": MessageTypeBySchemaName["apollo.common.Point3D"],
    "rotation": MessageTypeBySchemaName["apollo.common.Quaternion"],
  },
  "apollo.common.Point3D": {
    "x": number,
    "y": number,
    "z": number,
  },
  "apollo.common.Quaternion": {
    "qx": number,
    "qy": number,
    "qz": number,
    "qw": number,
  },
  "apollo.canbus.License": {
    "vin": string,
  },
  "apollo.common.VehicleID": {
    "vin": string,
    "plate": string,
    "other_unique_id": string,
  },
  "apollo.canbus.CheckResponse": {
    "is_eps_online": boolean,
    "is_epb_online": boolean,
    "is_esp_online": boolean,
    "is_vtog_online": boolean,
    "is_scu_online": boolean,
    "is_switch_online": boolean,
    "is_vcu_online": boolean,
  },
  "apollo.control.ControlCommand": {
    "header": MessageTypeBySchemaName["apollo.common.Header"],
    "throttle": number,
    "brake": number,
    "steering_rate": number,
    "steering_target": number,
    "parking_brake": boolean,
    "speed": number,
    "acceleration": number,
    "reset_model": boolean,
    "engine_on_off": boolean,
    "trajectory_fraction": number,
    // COMPLETE_MANUAL = undefined
    // COMPLETE_AUTO_DRIVE = undefined
    // AUTO_STEER_ONLY = undefined
    // AUTO_SPEED_ONLY = undefined
    // EMERGENCY_MODE = undefined
    "driving_mode": number,
    // GEAR_NEUTRAL = undefined
    // GEAR_DRIVE = undefined
    // GEAR_REVERSE = undefined
    // GEAR_PARKING = undefined
    // GEAR_LOW = undefined
    // GEAR_INVALID = undefined
    // GEAR_NONE = undefined
    "gear_location": number,
    "debug": MessageTypeBySchemaName["apollo.control.Debug"],
    "signal": MessageTypeBySchemaName["apollo.common.VehicleSignal"],
    "latency_stats": MessageTypeBySchemaName["apollo.control.LatencyStats"],
    "pad_msg": MessageTypeBySchemaName["apollo.control.PadMessage"],
    "engage_advice": MessageTypeBySchemaName["apollo.common.EngageAdvice"],
    "is_in_safe_mode": boolean,
    "left_turn": boolean,
    "right_turn": boolean,
    "high_beam": boolean,
    "low_beam": boolean,
    "horn": boolean,
    // TURN_NONE = undefined
    // TURN_LEFT = undefined
    // TURN_RIGHT = undefined
    "turnsignal": number,
  },
  "apollo.control.Debug": {
    "simple_lon_debug": MessageTypeBySchemaName["apollo.control.SimpleLongitudinalDebug"],
    "simple_lat_debug": MessageTypeBySchemaName["apollo.control.SimpleLateralDebug"],
    "input_debug": MessageTypeBySchemaName["apollo.control.InputDebug"],
    "simple_mpc_debug": MessageTypeBySchemaName["apollo.control.SimpleMPCDebug"],
  },
  "apollo.control.SimpleLongitudinalDebug": {
    "station_reference": number,
    "station_error": number,
    "station_error_limited": number,
    "preview_station_error": number,
    "speed_reference": number,
    "speed_error": number,
    "speed_controller_input_limited": number,
    "preview_speed_reference": number,
    "preview_speed_error": number,
    "preview_acceleration_reference": number,
    "acceleration_cmd_closeloop": number,
    "acceleration_cmd": number,
    "acceleration_lookup": number,
    "speed_lookup": number,
    "calibration_value": number,
    "throttle_cmd": number,
    "brake_cmd": number,
    "is_full_stop": boolean,
    "slope_offset_compensation": number,
    "current_station": number,
    "path_remain": number,
    "pid_saturation_status": number,
    "leadlag_saturation_status": number,
    "speed_offset": number,
    "current_speed": number,
    "acceleration_reference": number,
    "current_acceleration": number,
    "acceleration_error": number,
    "jerk_reference": number,
    "current_jerk": number,
    "jerk_error": number,
    "current_matched_point": MessageTypeBySchemaName["apollo.common.TrajectoryPoint"],
    "current_reference_point": MessageTypeBySchemaName["apollo.common.TrajectoryPoint"],
    "preview_reference_point": MessageTypeBySchemaName["apollo.common.TrajectoryPoint"],
    "acceleration_lookup_limit": number,
  },
  "apollo.common.TrajectoryPoint": {
    "path_point": MessageTypeBySchemaName["apollo.common.PathPoint"],
    "v": number,
    "a": number,
    "relative_time": number,
    "da": number,
    "steer": number,
    "gaussian_info": MessageTypeBySchemaName["apollo.common.GaussianInfo"],
  },
  "apollo.common.PathPoint": {
    "x": number,
    "y": number,
    "z": number,
    "theta": number,
    "kappa": number,
    "s": number,
    "dkappa": number,
    "ddkappa": number,
    "lane_id": string,
    "x_derivative": number,
    "y_derivative": number,
  },
  "apollo.common.GaussianInfo": {
    "sigma_x": number,
    "sigma_y": number,
    "correlation": number,
    "area_probability": number,
    "ellipse_a": number,
    "ellipse_b": number,
    "theta_a": number,
  },
  "apollo.control.SimpleLateralDebug": {
    "lateral_error": number,
    "ref_heading": number,
    "heading": number,
    "heading_error": number,
    "heading_error_rate": number,
    "lateral_error_rate": number,
    "curvature": number,
    "steer_angle": number,
    "steer_angle_feedforward": number,
    "steer_angle_lateral_contribution": number,
    "steer_angle_lateral_rate_contribution": number,
    "steer_angle_heading_contribution": number,
    "steer_angle_heading_rate_contribution": number,
    "steer_angle_feedback": number,
    "steering_position": number,
    "ref_speed": number,
    "steer_angle_limited": number,
    "lateral_acceleration": number,
    "lateral_jerk": number,
    "ref_heading_rate": number,
    "heading_rate": number,
    "ref_heading_acceleration": number,
    "heading_acceleration": number,
    "heading_error_acceleration": number,
    "ref_heading_jerk": number,
    "heading_jerk": number,
    "heading_error_jerk": number,
    "lateral_error_feedback": number,
    "heading_error_feedback": number,
    "current_target_point": MessageTypeBySchemaName["apollo.common.TrajectoryPoint"],
    "steer_angle_feedback_augment": number,
    "steer_mrac_debug": MessageTypeBySchemaName["apollo.control.MracDebug"],
    "steer_mrac_enable_status": boolean,
  },
  "apollo.control.MracDebug": {
    "mrac_model_order": number,
    "mrac_reference_state": Float64Array,
    "mrac_state_error": Float64Array,
    "mrac_adaptive_gain": MessageTypeBySchemaName["apollo.control.MracAdaptiveGain"],
    "mrac_reference_saturation_status": number,
    "mrac_control_saturation_status": number,
  },
  "apollo.control.MracAdaptiveGain": {
    "state_adaptive_gain": Float64Array,
    "input_adaptive_gain": Float64Array,
    "nonlinear_adaptive_gain": Float64Array,
  },
  "apollo.control.InputDebug": {
    "localization_header": MessageTypeBySchemaName["apollo.common.Header"],
    "canbus_header": MessageTypeBySchemaName["apollo.common.Header"],
    "trajectory_header": MessageTypeBySchemaName["apollo.common.Header"],
    "latest_replan_trajectory_header": MessageTypeBySchemaName["apollo.common.Header"],
  },
  "apollo.control.SimpleMPCDebug": {
    "lateral_error": number,
    "ref_heading": number,
    "heading": number,
    "heading_error": number,
    "heading_error_rate": number,
    "lateral_error_rate": number,
    "curvature": number,
    "steer_angle": number,
    "steer_angle_feedforward": number,
    "steer_angle_lateral_contribution": number,
    "steer_angle_lateral_rate_contribution": number,
    "steer_angle_heading_contribution": number,
    "steer_angle_heading_rate_contribution": number,
    "steer_angle_feedback": number,
    "steering_position": number,
    "ref_speed": number,
    "steer_angle_limited": number,
    "station_reference": number,
    "station_error": number,
    "speed_reference": number,
    "speed_error": number,
    "acceleration_reference": number,
    "is_full_stop": boolean,
    "station_feedback": number,
    "speed_feedback": number,
    "acceleration_cmd_closeloop": number,
    "acceleration_cmd": number,
    "acceleration_lookup": number,
    "speed_lookup": number,
    "calibration_value": number,
    "steer_unconstrained_control_diff": number,
    "steer_angle_feedforward_compensation": number,
    "matrix_q_updated": Float64Array,
    "matrix_r_updated": Float64Array,
    "lateral_acceleration": number,
    "lateral_jerk": number,
    "ref_heading_rate": number,
    "heading_rate": number,
    "ref_heading_acceleration": number,
    "heading_acceleration": number,
    "heading_error_acceleration": number,
    "ref_heading_jerk": number,
    "heading_jerk": number,
    "heading_error_jerk": number,
    "acceleration_feedback": number,
    "acceleration_error": number,
    "jerk_reference": number,
    "jerk_feedback": number,
    "jerk_error": number,
  },
  "apollo.control.LatencyStats": {
    "total_time_ms": number,
    "controller_time_ms": Float64Array,
    "total_time_exceeded": boolean,
  },
  "apollo.control.PadMessage": {
    "header": MessageTypeBySchemaName["apollo.common.Header"],
    // COMPLETE_MANUAL = undefined
    // COMPLETE_AUTO_DRIVE = undefined
    // AUTO_STEER_ONLY = undefined
    // AUTO_SPEED_ONLY = undefined
    // EMERGENCY_MODE = undefined
    "driving_mode": number,
    // STOP = undefined
    // START = undefined
    // RESET = undefined
    // VIN_REQ = undefined
    "action": number,
  },
  "apollo.dreamview.HMIStatus": {
    "header": MessageTypeBySchemaName["apollo.common.Header"],
    "modes": string[],
    "current_mode": string,
    "maps": string[],
    "current_map": string,
    "vehicles": string[],
    "current_vehicle": string,
    "modules": MessageTypeBySchemaName["apollo.dreamview.HMIStatus.ModulesEntry"][],
    "monitored_components": MessageTypeBySchemaName["apollo.dreamview.HMIStatus.MonitoredComponentsEntry"][],
    "docker_image": string,
    "utm_zone_id": number,
    "passenger_msg": string,
    "other_components": MessageTypeBySchemaName["apollo.dreamview.HMIStatus.OtherComponentsEntry"][],
    "scenario_set": MessageTypeBySchemaName["apollo.dreamview.HMIStatus.ScenarioSetEntry"][],
    "current_scenario_set_id": string,
    "current_scenario_id": string,
    "dynamic_models": string[],
    "current_dynamic_model": string,
    "current_record_id": string,
    "records": MessageTypeBySchemaName["apollo.dreamview.HMIStatus.RecordsEntry"][],
    "current_vehicle_type": number,
    "current_camera_sensor_channel": string,
    "current_point_cloud_channel": string,
  },
  "apollo.dreamview.HMIStatus.ModulesEntry": {
    "key": string,
    "value": boolean,
  },
  "apollo.dreamview.HMIStatus.MonitoredComponentsEntry": {
    "key": string,
    "value": MessageTypeBySchemaName["apollo.monitor.ComponentStatus"],
  },
  "apollo.monitor.ComponentStatus": {
    // UNKNOWN = undefined
    // OK = undefined
    // WARN = undefined
    // ERROR = undefined
    // FATAL = undefined
    "status": number,
    "message": string,
  },
  "apollo.dreamview.HMIStatus.OtherComponentsEntry": {
    "key": string,
    "value": MessageTypeBySchemaName["apollo.monitor.ComponentStatus"],
  },
  "apollo.dreamview.HMIStatus.ScenarioSetEntry": {
    "key": string,
    "value": MessageTypeBySchemaName["apollo.dreamview.ScenarioSet"],
  },
  "apollo.dreamview.ScenarioSet": {
    "scenario_set_name": string,
    "scenarios": MessageTypeBySchemaName["apollo.dreamview.ScenarioInfo"][],
  },
  "apollo.dreamview.ScenarioInfo": {
    "scenario_id": string,
    "scenario_name": string,
    "map_name": string,
    "start_point": MessageTypeBySchemaName["apollo.common.Point2D"],
  },
  "apollo.common.Point2D": {
    "x": number,
    "y": number,
  },
  "apollo.dreamview.HMIStatus.RecordsEntry": {
    "key": string,
    "value": number,
  },
  "apollo.localization.LocalizationStatus": {
    "header": MessageTypeBySchemaName["apollo.common.Header"],
    // OK = undefined
    // WARNNING = undefined
    // ERROR = undefined
    // CRITICAL_ERROR = undefined
    // FATAL_ERROR = undefined
    "fusion_status": number,
    // OK = undefined
    // WARNNING = undefined
    // ERROR = undefined
    // CRITICAL_ERROR = undefined
    // FATAL_ERROR = undefined
    "gnss_status": number,
    // OK = undefined
    // WARNNING = undefined
    // ERROR = undefined
    // CRITICAL_ERROR = undefined
    // FATAL_ERROR = undefined
    "lidar_status": number,
    "measurement_time": number,
    "state_message": string,
  },
  "apollo.localization.LocalizationEstimate": {
    "header": MessageTypeBySchemaName["apollo.common.Header"],
    "pose": MessageTypeBySchemaName["apollo.localization.Pose"],
    "uncertainty": MessageTypeBySchemaName["apollo.localization.Uncertainty"],
    "measurement_time": number,
    "trajectory_point": MessageTypeBySchemaName["apollo.common.TrajectoryPoint"][],
    "msf_status": MessageTypeBySchemaName["apollo.localization.MsfStatus"],
    "sensor_status": MessageTypeBySchemaName["apollo.localization.MsfSensorMsgStatus"],
  },
  "apollo.localization.Pose": {
    "position": MessageTypeBySchemaName["apollo.common.PointENU"],
    "orientation": MessageTypeBySchemaName["apollo.common.Quaternion"],
    "linear_velocity": MessageTypeBySchemaName["apollo.common.Point3D"],
    "linear_acceleration": MessageTypeBySchemaName["apollo.common.Point3D"],
    "angular_velocity": MessageTypeBySchemaName["apollo.common.Point3D"],
    "heading": number,
    "linear_acceleration_vrf": MessageTypeBySchemaName["apollo.common.Point3D"],
    "angular_velocity_vrf": MessageTypeBySchemaName["apollo.common.Point3D"],
    "euler_angles": MessageTypeBySchemaName["apollo.common.Point3D"],
  },
  "apollo.common.PointENU": {
    "x": number,
    "y": number,
    "z": number,
  },
  "apollo.localization.Uncertainty": {
    "position_std_dev": MessageTypeBySchemaName["apollo.common.Point3D"],
    "orientation_std_dev": MessageTypeBySchemaName["apollo.common.Point3D"],
    "linear_velocity_std_dev": MessageTypeBySchemaName["apollo.common.Point3D"],
    "linear_acceleration_std_dev": MessageTypeBySchemaName["apollo.common.Point3D"],
    "angular_velocity_std_dev": MessageTypeBySchemaName["apollo.common.Point3D"],
  },
  "apollo.localization.MsfStatus": {
    // MSF_LOCAL_LIDAR_CONSISTENCY_00 = undefined
    // MSF_LOCAL_LIDAR_CONSISTENCY_01 = undefined
    // MSF_LOCAL_LIDAR_CONSISTENCY_02 = undefined
    // MSF_LOCAL_LIDAR_CONSISTENCY_03 = undefined
    "local_lidar_consistency": number,
    // MSF_GNSS_CONSISTENCY_00 = undefined
    // MSF_GNSS_CONSISTENCY_01 = undefined
    // MSF_GNSS_CONSISTENCY_02 = undefined
    // MSF_GNSS_CONSISTENCY_03 = undefined
    "gnss_consistency": number,
    // MSF_LOCAL_LIDAR_NORMAL = undefined
    // MSF_LOCAL_LIDAR_MAP_MISSING = undefined
    // MSF_LOCAL_LIDAR_EXTRINSICS_MISSING = undefined
    // MSF_LOCAL_LIDAR_MAP_LOADING_FAILED = undefined
    // MSF_LOCAL_LIDAR_NO_OUTPUT = undefined
    // MSF_LOCAL_LIDAR_OUT_OF_MAP = undefined
    // MSF_LOCAL_LIDAR_NOT_GOOD = undefined
    // MSF_LOCAL_LIDAR_UNDEFINED_STATUS = undefined
    "local_lidar_status": number,
    // MSF_LOCAL_LIDAR_VERY_GOOD = undefined
    // MSF_LOCAL_LIDAR_GOOD = undefined
    // MSF_LOCAL_LIDAR_NOT_BAD = undefined
    // MSF_LOCAL_LIDAR_BAD = undefined
    "local_lidar_quality": number,
    // NONE = undefined
    // FIXEDPOS = undefined
    // FIXEDHEIGHT = undefined
    // FLOATCONV = undefined
    // WIDELANE = undefined
    // NARROWLANE = undefined
    // DOPPLER_VELOCITY = undefined
    // SINGLE = undefined
    // PSRDIFF = undefined
    // WAAS = undefined
    // PROPOGATED = undefined
    // OMNISTAR = undefined
    // L1_FLOAT = undefined
    // IONOFREE_FLOAT = undefined
    // NARROW_FLOAT = undefined
    // L1_INT = undefined
    // WIDE_INT = undefined
    // NARROW_INT = undefined
    // RTK_DIRECT_INS = undefined
    // INS_SBAS = undefined
    // INS_PSRSP = undefined
    // INS_PSRDIFF = undefined
    // INS_RTKFLOAT = undefined
    // INS_RTKFIXED = undefined
    // INS_OMNISTAR = undefined
    // INS_OMNISTAR_HP = undefined
    // INS_OMNISTAR_XP = undefined
    // OMNISTAR_HP = undefined
    // OMNISTAR_XP = undefined
    // PPP_CONVERGING = undefined
    // PPP = undefined
    // INS_PPP_Converging = undefined
    // INS_PPP = undefined
    // MSG_LOSS = undefined
    "gnsspos_position_type": number,
    // MSF_SOL_LIDAR_GNSS = undefined
    // MSF_SOL_X_GNSS = undefined
    // MSF_SOL_LIDAR_X = undefined
    // MSF_SOL_LIDAR_XX = undefined
    // MSF_SOL_LIDAR_XXX = undefined
    // MSF_SOL_X_X = undefined
    // MSF_SOL_X_XX = undefined
    // MSF_SOL_X_XXX = undefined
    // MSF_SSOL_LIDAR_GNSS = undefined
    // MSF_SSOL_X_GNSS = undefined
    // MSF_SSOL_LIDAR_X = undefined
    // MSF_SSOL_LIDAR_XX = undefined
    // MSF_SSOL_LIDAR_XXX = undefined
    // MSF_SSOL_X_X = undefined
    // MSF_SSOL_X_XX = undefined
    // MSF_SSOL_X_XXX = undefined
    // MSF_NOSOL_LIDAR_GNSS = undefined
    // MSF_NOSOL_X_GNSS = undefined
    // MSF_NOSOL_LIDAR_X = undefined
    // MSF_NOSOL_LIDAR_XX = undefined
    // MSF_NOSOL_LIDAR_XXX = undefined
    // MSF_NOSOL_X_X = undefined
    // MSF_NOSOL_X_XX = undefined
    // MSF_NOSOL_X_XXX = undefined
    // MSF_RUNNING_INIT = undefined
    "msf_running_status": number,
  },
  "apollo.localization.MsfSensorMsgStatus": {
    // IMU_DELAY_NORMAL = undefined
    // IMU_DELAY_1 = undefined
    // IMU_DELAY_2 = undefined
    // IMU_DELAY_3 = undefined
    // IMU_DELAY_ABNORMAL = undefined
    "imu_delay_status": number,
    // IMU_MISSING_NORMAL = undefined
    // IMU_MISSING_1 = undefined
    // IMU_MISSING_2 = undefined
    // IMU_MISSING_3 = undefined
    // IMU_MISSING_4 = undefined
    // IMU_MISSING_5 = undefined
    // IMU_MISSING_ABNORMAL = undefined
    "imu_missing_status": number,
    // IMU_DATA_NORMAL = undefined
    // IMU_DATA_ABNORMAL = undefined
    // IMU_DATA_OTHER = undefined
    "imu_data_status": number,
  },
  "apollo.planning.ADCTrajectory": {
    "header": MessageTypeBySchemaName["apollo.common.Header"],
    "total_path_length": number,
    "total_path_time": number,
    "estop": MessageTypeBySchemaName["apollo.planning.EStop"],
    "debug": MessageTypeBySchemaName["apollo.planning_internal.Debug"],
    "is_replan": boolean,
    // GEAR_NEUTRAL = undefined
    // GEAR_DRIVE = undefined
    // GEAR_REVERSE = undefined
    // GEAR_PARKING = undefined
    // GEAR_LOW = undefined
    // GEAR_INVALID = undefined
    // GEAR_NONE = undefined
    "gear": number,
    "trajectory_point": MessageTypeBySchemaName["apollo.common.TrajectoryPoint"][],
    "path_point": MessageTypeBySchemaName["apollo.common.PathPoint"][],
    "decision": MessageTypeBySchemaName["apollo.planning.DecisionResult"],
    "latency_stats": MessageTypeBySchemaName["apollo.planning.LatencyStats"],
    "routing_header": MessageTypeBySchemaName["apollo.common.Header"],
    // UNPROTECTED = undefined
    // PROTECTED = undefined
    "right_of_way_status": number,
    "lane_id": MessageTypeBySchemaName["apollo.hdmap.Id"][],
    "engage_advice": MessageTypeBySchemaName["apollo.common.EngageAdvice"],
    "critical_region": MessageTypeBySchemaName["apollo.planning.ADCTrajectory.CriticalRegion"],
    // UNKNOWN = undefined
    // NORMAL = undefined
    // PATH_FALLBACK = undefined
    // SPEED_FALLBACK = undefined
    // PATH_REUSED = undefined
    "trajectory_type": number,
    "replan_reason": string,
    "target_lane_id": MessageTypeBySchemaName["apollo.hdmap.Id"][],
    "car_in_dead_end": boolean,
    "rss_info": MessageTypeBySchemaName["apollo.planning.RSSInfo"],
  },
  "apollo.planning.EStop": {
    "is_estop": boolean,
    "reason": string,
  },
  "apollo.planning_internal.Debug": {
    "planning_data": MessageTypeBySchemaName["apollo.planning_internal.PlanningData"],
  },
  "apollo.planning_internal.PlanningData": {
    "adc_position": MessageTypeBySchemaName["apollo.localization.LocalizationEstimate"],
    "chassis": MessageTypeBySchemaName["apollo.canbus.Chassis"],
    "routing": MessageTypeBySchemaName["apollo.routing.RoutingResponse"],
    "init_point": MessageTypeBySchemaName["apollo.common.TrajectoryPoint"],
    "path": MessageTypeBySchemaName["apollo.common.Path"][],
    "speed_plan": MessageTypeBySchemaName["apollo.planning_internal.SpeedPlan"][],
    "st_graph": MessageTypeBySchemaName["apollo.planning_internal.STGraphDebug"][],
    "sl_frame": MessageTypeBySchemaName["apollo.planning_internal.SLFrameDebug"][],
    "prediction_header": MessageTypeBySchemaName["apollo.common.Header"],
    "signal_light": MessageTypeBySchemaName["apollo.planning_internal.SignalLightDebug"],
    "obstacle": MessageTypeBySchemaName["apollo.planning_internal.ObstacleDebug"][],
    "reference_line": MessageTypeBySchemaName["apollo.planning_internal.ReferenceLineDebug"][],
    "dp_poly_graph": MessageTypeBySchemaName["apollo.planning_internal.DpPolyGraphDebug"],
    "lattice_st_image": MessageTypeBySchemaName["apollo.planning_internal.LatticeStTraining"],
    "relative_map": MessageTypeBySchemaName["apollo.relative_map.MapMsg"],
    "auto_tuning_training_data": MessageTypeBySchemaName["apollo.planning_internal.AutoTuningTrainingData"],
    "front_clear_distance": number,
    "chart": MessageTypeBySchemaName["apollo.dreamview.Chart"][],
    "scenario": MessageTypeBySchemaName["apollo.planning_internal.ScenarioDebug"],
    "open_space": MessageTypeBySchemaName["apollo.planning_internal.OpenSpaceDebug"],
    "smoother": MessageTypeBySchemaName["apollo.planning_internal.SmootherDebug"],
    "pull_over": MessageTypeBySchemaName["apollo.planning_internal.PullOverDebug"],
    "hybrid_model": MessageTypeBySchemaName["apollo.planning_internal.HybridModelDebug"],
  },
  "apollo.routing.RoutingResponse": {
    "header": MessageTypeBySchemaName["apollo.common.Header"],
    "road": MessageTypeBySchemaName["apollo.routing.RoadSegment"][],
    "measurement": MessageTypeBySchemaName["apollo.routing.Measurement"],
    "routing_request": MessageTypeBySchemaName["apollo.routing.RoutingRequest"],
    "map_version": Uint8Array,
    "status": MessageTypeBySchemaName["apollo.common.StatusPb"],
  },
  "apollo.routing.RoadSegment": {
    "id": string,
    "passage": MessageTypeBySchemaName["apollo.routing.Passage"][],
  },
  "apollo.routing.Passage": {
    "segment": MessageTypeBySchemaName["apollo.routing.LaneSegment"][],
    "can_exit": boolean,
    // FORWARD = undefined
    // LEFT = undefined
    // RIGHT = undefined
    "change_lane_type": number,
  },
  "apollo.routing.LaneSegment": {
    "id": string,
    "start_s": number,
    "end_s": number,
  },
  "apollo.routing.Measurement": {
    "distance": number,
  },
  "apollo.routing.RoutingRequest": {
    "header": MessageTypeBySchemaName["apollo.common.Header"],
    "waypoint": MessageTypeBySchemaName["apollo.routing.LaneWaypoint"][],
    "blacklisted_lane": MessageTypeBySchemaName["apollo.routing.LaneSegment"][],
    "blacklisted_road": string[],
    "broadcast": boolean,
    "parking_space": MessageTypeBySchemaName["apollo.hdmap.ParkingSpace"],
    "parking_info": MessageTypeBySchemaName["apollo.routing.ParkingInfo"],
    "dead_end_info": MessageTypeBySchemaName["apollo.routing.DeadEndInfo"],
  },
  "apollo.routing.LaneWaypoint": {
    "id": string,
    "s": number,
    "pose": MessageTypeBySchemaName["apollo.common.PointENU"],
    "heading": number,
  },
  "apollo.hdmap.ParkingSpace": {
    "id": MessageTypeBySchemaName["apollo.hdmap.Id"],
    "polygon": MessageTypeBySchemaName["apollo.hdmap.Polygon"],
    "overlap_id": MessageTypeBySchemaName["apollo.hdmap.Id"][],
    "heading": number,
  },
  "apollo.hdmap.Id": {
    "id": string,
  },
  "apollo.hdmap.Polygon": {
    "point": MessageTypeBySchemaName["apollo.common.PointENU"][],
  },
  "apollo.routing.ParkingInfo": {
    "parking_space_id": string,
    "parking_point": MessageTypeBySchemaName["apollo.common.PointENU"],
    // VERTICAL_PLOT = undefined
    // PARALLEL_PARKING = undefined
    "parking_space_type": number,
    "corner_point": MessageTypeBySchemaName["apollo.hdmap.Polygon"],
  },
  "apollo.routing.DeadEndInfo": {
    // ROUTING_OTHER = undefined
    // ROUTING_IN = undefined
    // ROUTING_OUT = undefined
    "dead_end_routing_type": number,
    "target_point": MessageTypeBySchemaName["apollo.common.PointENU"],
  },
  "apollo.common.Path": {
    "name": string,
    "path_point": MessageTypeBySchemaName["apollo.common.PathPoint"][],
  },
  "apollo.planning_internal.SpeedPlan": {
    "name": string,
    "speed_point": MessageTypeBySchemaName["apollo.common.SpeedPoint"][],
  },
  "apollo.common.SpeedPoint": {
    "s": number,
    "t": number,
    "v": number,
    "a": number,
    "da": number,
  },
  "apollo.planning_internal.STGraphDebug": {
    "name": string,
    "boundary": MessageTypeBySchemaName["apollo.planning_internal.StGraphBoundaryDebug"][],
    "speed_limit": MessageTypeBySchemaName["apollo.common.SpeedPoint"][],
    "speed_profile": MessageTypeBySchemaName["apollo.common.SpeedPoint"][],
    "speed_constraint": MessageTypeBySchemaName["apollo.planning_internal.STGraphDebug.STGraphSpeedConstraint"],
    "kernel_cruise_ref": MessageTypeBySchemaName["apollo.planning_internal.STGraphDebug.STGraphKernelCuiseRef"],
    "kernel_follow_ref": MessageTypeBySchemaName["apollo.planning_internal.STGraphDebug.STGraphKernelFollowRef"],
  },
  "apollo.planning_internal.StGraphBoundaryDebug": {
    "name": string,
    "point": MessageTypeBySchemaName["apollo.common.SpeedPoint"][],
    // ST_BOUNDARY_TYPE_UNKNOWN = undefined
    // ST_BOUNDARY_TYPE_STOP = undefined
    // ST_BOUNDARY_TYPE_FOLLOW = undefined
    // ST_BOUNDARY_TYPE_YIELD = undefined
    // ST_BOUNDARY_TYPE_OVERTAKE = undefined
    // ST_BOUNDARY_TYPE_KEEP_CLEAR = undefined
    // ST_BOUNDARY_TYPE_DRIVABLE_REGION = undefined
    "type": number,
  },
  "apollo.planning_internal.STGraphDebug.STGraphSpeedConstraint": {
    "t": Float64Array,
    "lower_bound": Float64Array,
    "upper_bound": Float64Array,
  },
  "apollo.planning_internal.STGraphDebug.STGraphKernelCuiseRef": {
    "t": Float64Array,
    "cruise_line_s": Float64Array,
  },
  "apollo.planning_internal.STGraphDebug.STGraphKernelFollowRef": {
    "t": Float64Array,
    "follow_line_s": Float64Array,
  },
  "apollo.planning_internal.SLFrameDebug": {
    "name": string,
    "sampled_s": Float64Array,
    "static_obstacle_lower_bound": Float64Array,
    "dynamic_obstacle_lower_bound": Float64Array,
    "static_obstacle_upper_bound": Float64Array,
    "dynamic_obstacle_upper_bound": Float64Array,
    "map_lower_bound": Float64Array,
    "map_upper_bound": Float64Array,
    "sl_path": MessageTypeBySchemaName["apollo.common.SLPoint"][],
    "aggregated_boundary_s": Float64Array,
    "aggregated_boundary_low": Float64Array,
    "aggregated_boundary_high": Float64Array,
  },
  "apollo.common.SLPoint": {
    "s": number,
    "l": number,
  },
  "apollo.planning_internal.SignalLightDebug": {
    "adc_speed": number,
    "adc_front_s": number,
    "signal": MessageTypeBySchemaName["apollo.planning_internal.SignalLightDebug.SignalDebug"][],
  },
  "apollo.planning_internal.SignalLightDebug.SignalDebug": {
    "light_id": string,
    // UNKNOWN = undefined
    // RED = undefined
    // YELLOW = undefined
    // GREEN = undefined
    // BLACK = undefined
    "color": number,
    "light_stop_s": number,
    "adc_stop_deceleration": number,
    "is_stop_wall_created": boolean,
  },
  "apollo.planning_internal.ObstacleDebug": {
    "id": string,
    "sl_boundary": MessageTypeBySchemaName["apollo.planning.SLBoundary"],
    "decision_tag": MessageTypeBySchemaName["apollo.planning_internal.DecisionTag"][],
    "vertices_x_coords": Float64Array,
    "vertices_y_coords": Float64Array,
  },
  "apollo.planning.SLBoundary": {
    "start_s": number,
    "end_s": number,
    "start_l": number,
    "end_l": number,
    "boundary_point": MessageTypeBySchemaName["apollo.common.SLPoint"][],
  },
  "apollo.planning_internal.DecisionTag": {
    "decider_tag": string,
    "decision": MessageTypeBySchemaName["apollo.planning.ObjectDecisionType"],
  },
  "apollo.planning.ObjectDecisionType": {
    "ignore": MessageTypeBySchemaName["apollo.planning.ObjectIgnore"],
    "stop": MessageTypeBySchemaName["apollo.planning.ObjectStop"],
    "follow": MessageTypeBySchemaName["apollo.planning.ObjectFollow"],
    "yield": MessageTypeBySchemaName["apollo.planning.ObjectYield"],
    "overtake": MessageTypeBySchemaName["apollo.planning.ObjectOvertake"],
    "nudge": MessageTypeBySchemaName["apollo.planning.ObjectNudge"],
    "avoid": MessageTypeBySchemaName["apollo.planning.ObjectAvoid"],
    "side_pass": MessageTypeBySchemaName["apollo.planning.ObjectSidePass"],
  },
  "apollo.planning.ObjectIgnore": {
  },
  "apollo.planning.ObjectStop": {
    // STOP_REASON_HEAD_VEHICLE = undefined
    // STOP_REASON_DESTINATION = undefined
    // STOP_REASON_PEDESTRIAN = undefined
    // STOP_REASON_OBSTACLE = undefined
    // STOP_REASON_PREPARKING = undefined
    // STOP_REASON_SIGNAL = undefined
    // STOP_REASON_STOP_SIGN = undefined
    // STOP_REASON_YIELD_SIGN = undefined
    // STOP_REASON_CLEAR_ZONE = undefined
    // STOP_REASON_CROSSWALK = undefined
    // STOP_REASON_CREEPER = undefined
    // STOP_REASON_REFERENCE_END = undefined
    // STOP_REASON_YELLOW_SIGNAL = undefined
    // STOP_REASON_PULL_OVER = undefined
    // STOP_REASON_SIDEPASS_SAFETY = undefined
    // STOP_REASON_PRE_OPEN_SPACE_STOP = undefined
    // STOP_REASON_LANE_CHANGE_URGENCY = undefined
    // STOP_REASON_EMERGENCY = undefined
    "reason_code": number,
    "distance_s": number,
    "stop_point": MessageTypeBySchemaName["apollo.common.PointENU"],
    "stop_heading": number,
    "wait_for_obstacle": string[],
  },
  "apollo.planning.ObjectFollow": {
    "distance_s": number,
    "fence_point": MessageTypeBySchemaName["apollo.common.PointENU"],
    "fence_heading": number,
  },
  "apollo.planning.ObjectYield": {
    "distance_s": number,
    "fence_point": MessageTypeBySchemaName["apollo.common.PointENU"],
    "fence_heading": number,
    "time_buffer": number,
  },
  "apollo.planning.ObjectOvertake": {
    "distance_s": number,
    "fence_point": MessageTypeBySchemaName["apollo.common.PointENU"],
    "fence_heading": number,
    "time_buffer": number,
  },
  "apollo.planning.ObjectNudge": {
    // LEFT_NUDGE = undefined
    // RIGHT_NUDGE = undefined
    // DYNAMIC_LEFT_NUDGE = undefined
    // DYNAMIC_RIGHT_NUDGE = undefined
    "type": number,
    "distance_l": number,
  },
  "apollo.planning.ObjectAvoid": {
  },
  "apollo.planning.ObjectSidePass": {
    // LEFT = undefined
    // RIGHT = undefined
    "type": number,
  },
  "apollo.planning_internal.ReferenceLineDebug": {
    "id": string,
    "length": number,
    "cost": number,
    "is_change_lane_path": boolean,
    "is_drivable": boolean,
    "is_protected": boolean,
    "is_offroad": boolean,
    "minimum_boundary": number,
    "average_kappa": number,
    "average_dkappa": number,
    "kappa_rms": number,
    "dkappa_rms": number,
    "kappa_max_abs": number,
    "dkappa_max_abs": number,
    "average_offset": number,
  },
  "apollo.planning_internal.DpPolyGraphDebug": {
    "sample_layer": MessageTypeBySchemaName["apollo.planning_internal.SampleLayerDebug"][],
    "min_cost_point": MessageTypeBySchemaName["apollo.common.SLPoint"][],
  },
  "apollo.planning_internal.SampleLayerDebug": {
    "sl_point": MessageTypeBySchemaName["apollo.common.SLPoint"][],
  },
  "apollo.planning_internal.LatticeStTraining": {
    "pixel": MessageTypeBySchemaName["apollo.planning_internal.LatticeStPixel"][],
    "timestamp": number,
    "annotation": string,
    "num_s_grids": number,
    "num_t_grids": number,
    "s_resolution": number,
    "t_resolution": number,
  },
  "apollo.planning_internal.LatticeStPixel": {
    "s": number,
    "t": number,
    "r": number,
    "g": number,
    "b": number,
  },
  "apollo.relative_map.MapMsg": {
    "header": MessageTypeBySchemaName["apollo.common.Header"],
    "hdmap": MessageTypeBySchemaName["apollo.hdmap.Map"],
    "navigation_path": MessageTypeBySchemaName["apollo.relative_map.MapMsg.NavigationPathEntry"][],
    "lane_marker": MessageTypeBySchemaName["apollo.perception.LaneMarkers"],
    "localization": MessageTypeBySchemaName["apollo.localization.LocalizationEstimate"],
  },
  "apollo.hdmap.Map": {
    "header": MessageTypeBySchemaName["apollo.hdmap.Header"],
    "crosswalk": MessageTypeBySchemaName["apollo.hdmap.Crosswalk"][],
    "junction": MessageTypeBySchemaName["apollo.hdmap.Junction"][],
    "lane": MessageTypeBySchemaName["apollo.hdmap.Lane"][],
    "stop_sign": MessageTypeBySchemaName["apollo.hdmap.StopSign"][],
    "signal": MessageTypeBySchemaName["apollo.hdmap.Signal"][],
    "yield": MessageTypeBySchemaName["apollo.hdmap.YieldSign"][],
    "overlap": MessageTypeBySchemaName["apollo.hdmap.Overlap"][],
    "clear_area": MessageTypeBySchemaName["apollo.hdmap.ClearArea"][],
    "speed_bump": MessageTypeBySchemaName["apollo.hdmap.SpeedBump"][],
    "road": MessageTypeBySchemaName["apollo.hdmap.Road"][],
    "parking_space": MessageTypeBySchemaName["apollo.hdmap.ParkingSpace"][],
    "pnc_junction": MessageTypeBySchemaName["apollo.hdmap.PNCJunction"][],
    "rsu": MessageTypeBySchemaName["apollo.hdmap.RSU"][],
  },
  "apollo.hdmap.Header": {
    "version": Uint8Array,
    "date": Uint8Array,
    "projection": MessageTypeBySchemaName["apollo.hdmap.Projection"],
    "district": Uint8Array,
    "generation": Uint8Array,
    "rev_major": Uint8Array,
    "rev_minor": Uint8Array,
    "left": number,
    "top": number,
    "right": number,
    "bottom": number,
    "vendor": Uint8Array,
  },
  "apollo.hdmap.Projection": {
    "proj": string,
  },
  "apollo.hdmap.Crosswalk": {
    "id": MessageTypeBySchemaName["apollo.hdmap.Id"],
    "polygon": MessageTypeBySchemaName["apollo.hdmap.Polygon"],
    "overlap_id": MessageTypeBySchemaName["apollo.hdmap.Id"][],
  },
  "apollo.hdmap.Junction": {
    "id": MessageTypeBySchemaName["apollo.hdmap.Id"],
    "polygon": MessageTypeBySchemaName["apollo.hdmap.Polygon"],
    "overlap_id": MessageTypeBySchemaName["apollo.hdmap.Id"][],
    // UNKNOWN = undefined
    // IN_ROAD = undefined
    // CROSS_ROAD = undefined
    // FORK_ROAD = undefined
    // MAIN_SIDE = undefined
    // DEAD_END = undefined
    "type": number,
  },
  "apollo.hdmap.Lane": {
    "id": MessageTypeBySchemaName["apollo.hdmap.Id"],
    "central_curve": MessageTypeBySchemaName["apollo.hdmap.Curve"],
    "left_boundary": MessageTypeBySchemaName["apollo.hdmap.LaneBoundary"],
    "right_boundary": MessageTypeBySchemaName["apollo.hdmap.LaneBoundary"],
    "length": number,
    "speed_limit": number,
    "overlap_id": MessageTypeBySchemaName["apollo.hdmap.Id"][],
    "predecessor_id": MessageTypeBySchemaName["apollo.hdmap.Id"][],
    "successor_id": MessageTypeBySchemaName["apollo.hdmap.Id"][],
    "left_neighbor_forward_lane_id": MessageTypeBySchemaName["apollo.hdmap.Id"][],
    "right_neighbor_forward_lane_id": MessageTypeBySchemaName["apollo.hdmap.Id"][],
    // NONE = undefined
    // CITY_DRIVING = undefined
    // BIKING = undefined
    // SIDEWALK = undefined
    // PARKING = undefined
    // SHOULDER = undefined
    "type": number,
    // NO_TURN = undefined
    // LEFT_TURN = undefined
    // RIGHT_TURN = undefined
    // U_TURN = undefined
    "turn": number,
    "left_neighbor_reverse_lane_id": MessageTypeBySchemaName["apollo.hdmap.Id"][],
    "right_neighbor_reverse_lane_id": MessageTypeBySchemaName["apollo.hdmap.Id"][],
    "junction_id": MessageTypeBySchemaName["apollo.hdmap.Id"],
    "left_sample": MessageTypeBySchemaName["apollo.hdmap.LaneSampleAssociation"][],
    "right_sample": MessageTypeBySchemaName["apollo.hdmap.LaneSampleAssociation"][],
    // FORWARD = undefined
    // BACKWARD = undefined
    // BIDIRECTION = undefined
    "direction": number,
    "left_road_sample": MessageTypeBySchemaName["apollo.hdmap.LaneSampleAssociation"][],
    "right_road_sample": MessageTypeBySchemaName["apollo.hdmap.LaneSampleAssociation"][],
    "self_reverse_lane_id": MessageTypeBySchemaName["apollo.hdmap.Id"][],
  },
  "apollo.hdmap.Curve": {
    "segment": MessageTypeBySchemaName["apollo.hdmap.CurveSegment"][],
  },
  "apollo.hdmap.CurveSegment": {
    "line_segment": MessageTypeBySchemaName["apollo.hdmap.LineSegment"],
    "s": number,
    "start_position": MessageTypeBySchemaName["apollo.common.PointENU"],
    "heading": number,
    "length": number,
  },
  "apollo.hdmap.LineSegment": {
    "point": MessageTypeBySchemaName["apollo.common.PointENU"][],
  },
  "apollo.hdmap.LaneBoundary": {
    "curve": MessageTypeBySchemaName["apollo.hdmap.Curve"],
    "length": number,
    "virtual": boolean,
    "boundary_type": MessageTypeBySchemaName["apollo.hdmap.LaneBoundaryType"][],
  },
  "apollo.hdmap.LaneBoundaryType": {
    "s": number,
    // UNKNOWN = undefined
    // DOTTED_YELLOW = undefined
    // DOTTED_WHITE = undefined
    // SOLID_YELLOW = undefined
    // SOLID_WHITE = undefined
    // DOUBLE_YELLOW = undefined
    // CURB = undefined
    "types": number,
  },
  "apollo.hdmap.LaneSampleAssociation": {
    "s": number,
    "width": number,
  },
  "apollo.hdmap.StopSign": {
    "id": MessageTypeBySchemaName["apollo.hdmap.Id"],
    "stop_line": MessageTypeBySchemaName["apollo.hdmap.Curve"][],
    "overlap_id": MessageTypeBySchemaName["apollo.hdmap.Id"][],
    // UNKNOWN = undefined
    // ONE_WAY = undefined
    // TWO_WAY = undefined
    // THREE_WAY = undefined
    // FOUR_WAY = undefined
    // ALL_WAY = undefined
    "type": number,
  },
  "apollo.hdmap.Signal": {
    "id": MessageTypeBySchemaName["apollo.hdmap.Id"],
    "boundary": MessageTypeBySchemaName["apollo.hdmap.Polygon"],
    "subsignal": MessageTypeBySchemaName["apollo.hdmap.Subsignal"][],
    "overlap_id": MessageTypeBySchemaName["apollo.hdmap.Id"][],
    // UNKNOWN = undefined
    // MIX_2_HORIZONTAL = undefined
    // MIX_2_VERTICAL = undefined
    // MIX_3_HORIZONTAL = undefined
    // MIX_3_VERTICAL = undefined
    // SINGLE = undefined
    "type": number,
    "stop_line": MessageTypeBySchemaName["apollo.hdmap.Curve"][],
    "sign_info": MessageTypeBySchemaName["apollo.hdmap.SignInfo"][],
  },
  "apollo.hdmap.Subsignal": {
    "id": MessageTypeBySchemaName["apollo.hdmap.Id"],
    // UNKNOWN = undefined
    // CIRCLE = undefined
    // ARROW_LEFT = undefined
    // ARROW_FORWARD = undefined
    // ARROW_RIGHT = undefined
    // ARROW_LEFT_AND_FORWARD = undefined
    // ARROW_RIGHT_AND_FORWARD = undefined
    // ARROW_U_TURN = undefined
    "type": number,
    "location": MessageTypeBySchemaName["apollo.common.PointENU"],
  },
  "apollo.hdmap.SignInfo": {
    // None = undefined
    // NO_RIGHT_TURN_ON_RED = undefined
    "type": number,
  },
  "apollo.hdmap.YieldSign": {
    "id": MessageTypeBySchemaName["apollo.hdmap.Id"],
    "stop_line": MessageTypeBySchemaName["apollo.hdmap.Curve"][],
    "overlap_id": MessageTypeBySchemaName["apollo.hdmap.Id"][],
  },
  "apollo.hdmap.Overlap": {
    "id": MessageTypeBySchemaName["apollo.hdmap.Id"],
    "object": MessageTypeBySchemaName["apollo.hdmap.ObjectOverlapInfo"][],
    "region_overlap": MessageTypeBySchemaName["apollo.hdmap.RegionOverlapInfo"][],
  },
  "apollo.hdmap.ObjectOverlapInfo": {
    "id": MessageTypeBySchemaName["apollo.hdmap.Id"],
    "lane_overlap_info": MessageTypeBySchemaName["apollo.hdmap.LaneOverlapInfo"],
    "signal_overlap_info": MessageTypeBySchemaName["apollo.hdmap.SignalOverlapInfo"],
    "stop_sign_overlap_info": MessageTypeBySchemaName["apollo.hdmap.StopSignOverlapInfo"],
    "crosswalk_overlap_info": MessageTypeBySchemaName["apollo.hdmap.CrosswalkOverlapInfo"],
    "junction_overlap_info": MessageTypeBySchemaName["apollo.hdmap.JunctionOverlapInfo"],
    "yield_sign_overlap_info": MessageTypeBySchemaName["apollo.hdmap.YieldOverlapInfo"],
    "clear_area_overlap_info": MessageTypeBySchemaName["apollo.hdmap.ClearAreaOverlapInfo"],
    "speed_bump_overlap_info": MessageTypeBySchemaName["apollo.hdmap.SpeedBumpOverlapInfo"],
    "parking_space_overlap_info": MessageTypeBySchemaName["apollo.hdmap.ParkingSpaceOverlapInfo"],
    "pnc_junction_overlap_info": MessageTypeBySchemaName["apollo.hdmap.PNCJunctionOverlapInfo"],
    "rsu_overlap_info": MessageTypeBySchemaName["apollo.hdmap.RSUOverlapInfo"],
  },
  "apollo.hdmap.LaneOverlapInfo": {
    "start_s": number,
    "end_s": number,
    "is_merge": boolean,
    "region_overlap_id": MessageTypeBySchemaName["apollo.hdmap.Id"],
  },
  "apollo.hdmap.SignalOverlapInfo": {
  },
  "apollo.hdmap.StopSignOverlapInfo": {
  },
  "apollo.hdmap.CrosswalkOverlapInfo": {
    "region_overlap_id": MessageTypeBySchemaName["apollo.hdmap.Id"],
  },
  "apollo.hdmap.JunctionOverlapInfo": {
  },
  "apollo.hdmap.YieldOverlapInfo": {
  },
  "apollo.hdmap.ClearAreaOverlapInfo": {
  },
  "apollo.hdmap.SpeedBumpOverlapInfo": {
  },
  "apollo.hdmap.ParkingSpaceOverlapInfo": {
  },
  "apollo.hdmap.PNCJunctionOverlapInfo": {
  },
  "apollo.hdmap.RSUOverlapInfo": {
  },
  "apollo.hdmap.RegionOverlapInfo": {
    "id": MessageTypeBySchemaName["apollo.hdmap.Id"],
    "polygon": MessageTypeBySchemaName["apollo.hdmap.Polygon"][],
  },
  "apollo.hdmap.ClearArea": {
    "id": MessageTypeBySchemaName["apollo.hdmap.Id"],
    "overlap_id": MessageTypeBySchemaName["apollo.hdmap.Id"][],
    "polygon": MessageTypeBySchemaName["apollo.hdmap.Polygon"],
  },
  "apollo.hdmap.SpeedBump": {
    "id": MessageTypeBySchemaName["apollo.hdmap.Id"],
    "overlap_id": MessageTypeBySchemaName["apollo.hdmap.Id"][],
    "position": MessageTypeBySchemaName["apollo.hdmap.Curve"][],
  },
  "apollo.hdmap.Road": {
    "id": MessageTypeBySchemaName["apollo.hdmap.Id"],
    "section": MessageTypeBySchemaName["apollo.hdmap.RoadSection"][],
    "junction_id": MessageTypeBySchemaName["apollo.hdmap.Id"],
    // UNKNOWN = undefined
    // HIGHWAY = undefined
    // CITY_ROAD = undefined
    // PARK = undefined
    "type": number,
  },
  "apollo.hdmap.RoadSection": {
    "id": MessageTypeBySchemaName["apollo.hdmap.Id"],
    "lane_id": MessageTypeBySchemaName["apollo.hdmap.Id"][],
    "boundary": MessageTypeBySchemaName["apollo.hdmap.RoadBoundary"],
  },
  "apollo.hdmap.RoadBoundary": {
    "outer_polygon": MessageTypeBySchemaName["apollo.hdmap.BoundaryPolygon"],
    "hole": MessageTypeBySchemaName["apollo.hdmap.BoundaryPolygon"][],
  },
  "apollo.hdmap.BoundaryPolygon": {
    "edge": MessageTypeBySchemaName["apollo.hdmap.BoundaryEdge"][],
  },
  "apollo.hdmap.BoundaryEdge": {
    "curve": MessageTypeBySchemaName["apollo.hdmap.Curve"],
    // UNKNOWN = undefined
    // NORMAL = undefined
    // LEFT_BOUNDARY = undefined
    // RIGHT_BOUNDARY = undefined
    "type": number,
  },
  "apollo.hdmap.PNCJunction": {
    "id": MessageTypeBySchemaName["apollo.hdmap.Id"],
    "polygon": MessageTypeBySchemaName["apollo.hdmap.Polygon"],
    "overlap_id": MessageTypeBySchemaName["apollo.hdmap.Id"][],
    "passage_group": MessageTypeBySchemaName["apollo.hdmap.PassageGroup"][],
  },
  "apollo.hdmap.PassageGroup": {
    "id": MessageTypeBySchemaName["apollo.hdmap.Id"],
    "passage": MessageTypeBySchemaName["apollo.hdmap.Passage"][],
  },
  "apollo.hdmap.Passage": {
    "id": MessageTypeBySchemaName["apollo.hdmap.Id"],
    "signal_id": MessageTypeBySchemaName["apollo.hdmap.Id"][],
    "yield_id": MessageTypeBySchemaName["apollo.hdmap.Id"][],
    "stop_sign_id": MessageTypeBySchemaName["apollo.hdmap.Id"][],
    "lane_id": MessageTypeBySchemaName["apollo.hdmap.Id"][],
    // UNKNOWN = undefined
    // ENTRANCE = undefined
    // EXIT = undefined
    "type": number,
  },
  "apollo.hdmap.RSU": {
    "id": MessageTypeBySchemaName["apollo.hdmap.Id"],
    "junction_id": MessageTypeBySchemaName["apollo.hdmap.Id"],
    "overlap_id": MessageTypeBySchemaName["apollo.hdmap.Id"][],
  },
  "apollo.relative_map.MapMsg.NavigationPathEntry": {
    "key": string,
    "value": MessageTypeBySchemaName["apollo.relative_map.NavigationPath"],
  },
  "apollo.relative_map.NavigationPath": {
    "path": MessageTypeBySchemaName["apollo.common.Path"],
    "path_priority": number,
  },
  "apollo.perception.LaneMarkers": {
    "left_lane_marker": MessageTypeBySchemaName["apollo.perception.LaneMarker"],
    "right_lane_marker": MessageTypeBySchemaName["apollo.perception.LaneMarker"],
    "next_left_lane_marker": MessageTypeBySchemaName["apollo.perception.LaneMarker"][],
    "next_right_lane_marker": MessageTypeBySchemaName["apollo.perception.LaneMarker"][],
  },
  "apollo.perception.LaneMarker": {
    // UNKNOWN = undefined
    // DOTTED_YELLOW = undefined
    // DOTTED_WHITE = undefined
    // SOLID_YELLOW = undefined
    // SOLID_WHITE = undefined
    // DOUBLE_YELLOW = undefined
    // CURB = undefined
    "lane_type": number,
    "quality": number,
    "model_degree": number,
    "c0_position": number,
    "c1_heading_angle": number,
    "c2_curvature": number,
    "c3_curvature_derivative": number,
    "view_range": number,
    "longitude_start": number,
    "longitude_end": number,
  },
  "apollo.planning_internal.AutoTuningTrainingData": {
    "teacher_component": MessageTypeBySchemaName["apollo.planning_internal.CostComponents"],
    "student_component": MessageTypeBySchemaName["apollo.planning_internal.CostComponents"],
  },
  "apollo.planning_internal.CostComponents": {
    "cost_component": Float64Array,
  },
  "apollo.dreamview.Chart": {
    "title": string,
    "options": MessageTypeBySchemaName["apollo.dreamview.Options"],
    "line": MessageTypeBySchemaName["apollo.dreamview.Line"][],
    "polygon": MessageTypeBySchemaName["apollo.dreamview.Polygon"][],
    "car": MessageTypeBySchemaName["apollo.dreamview.Car"][],
  },
  "apollo.dreamview.Options": {
    "legend_display": boolean,
    "x": MessageTypeBySchemaName["apollo.dreamview.Options.Axis"],
    "y": MessageTypeBySchemaName["apollo.dreamview.Options.Axis"],
    "aspect_ratio": number,
    "sync_xy_window_size": boolean,
  },
  "apollo.dreamview.Options.Axis": {
    "min": number,
    "max": number,
    "label_string": string,
    "window_size": number,
    "step_size": number,
    "mid_value": number,
  },
  "apollo.dreamview.Line": {
    "label": string,
    "hide_label_in_legend": boolean,
    "point": MessageTypeBySchemaName["apollo.common.Point2D"][],
    "properties": MessageTypeBySchemaName["apollo.dreamview.Line.PropertiesEntry"][],
  },
  "apollo.dreamview.Line.PropertiesEntry": {
    "key": string,
    "value": string,
  },
  "apollo.dreamview.Polygon": {
    "label": string,
    "hide_label_in_legend": boolean,
    "point": MessageTypeBySchemaName["apollo.common.Point2D"][],
    "properties": MessageTypeBySchemaName["apollo.dreamview.Polygon.PropertiesEntry"][],
  },
  "apollo.dreamview.Polygon.PropertiesEntry": {
    "key": string,
    "value": string,
  },
  "apollo.dreamview.Car": {
    "label": string,
    "hide_label_in_legend": boolean,
    "x": number,
    "y": number,
    "heading": number,
    "color": string,
  },
  "apollo.planning_internal.ScenarioDebug": {
    // LANE_FOLLOW = undefined
    // BARE_INTERSECTION_UNPROTECTED = undefined
    // STOP_SIGN_PROTECTED = undefined
    // STOP_SIGN_UNPROTECTED = undefined
    // TRAFFIC_LIGHT_PROTECTED = undefined
    // TRAFFIC_LIGHT_UNPROTECTED_LEFT_TURN = undefined
    // TRAFFIC_LIGHT_UNPROTECTED_RIGHT_TURN = undefined
    // YIELD_SIGN = undefined
    // PULL_OVER = undefined
    // VALET_PARKING = undefined
    // EMERGENCY_PULL_OVER = undefined
    // EMERGENCY_STOP = undefined
    // NARROW_STREET_U_TURN = undefined
    // PARK_AND_GO = undefined
    // LEARNING_MODEL_SAMPLE = undefined
    // DEADEND_TURNAROUND = undefined
    "scenario_type": number,
    // NO_STAGE = undefined
    // LANE_FOLLOW_DEFAULT_STAGE = undefined
    // BARE_INTERSECTION_UNPROTECTED_APPROACH = undefined
    // BARE_INTERSECTION_UNPROTECTED_INTERSECTION_CRUISE = undefined
    // STOP_SIGN_UNPROTECTED_PRE_STOP = undefined
    // STOP_SIGN_UNPROTECTED_STOP = undefined
    // STOP_SIGN_UNPROTECTED_CREEP = undefined
    // STOP_SIGN_UNPROTECTED_INTERSECTION_CRUISE = undefined
    // TRAFFIC_LIGHT_PROTECTED_APPROACH = undefined
    // TRAFFIC_LIGHT_PROTECTED_INTERSECTION_CRUISE = undefined
    // TRAFFIC_LIGHT_UNPROTECTED_LEFT_TURN_APPROACH = undefined
    // TRAFFIC_LIGHT_UNPROTECTED_LEFT_TURN_CREEP = undefined
    // TRAFFIC_LIGHT_UNPROTECTED_LEFT_TURN_INTERSECTION_CRUISE = undefined
    // TRAFFIC_LIGHT_UNPROTECTED_RIGHT_TURN_STOP = undefined
    // TRAFFIC_LIGHT_UNPROTECTED_RIGHT_TURN_CREEP = undefined
    // TRAFFIC_LIGHT_UNPROTECTED_RIGHT_TURN_INTERSECTION_CRUISE = undefined
    // PULL_OVER_APPROACH = undefined
    // PULL_OVER_RETRY_APPROACH_PARKING = undefined
    // PULL_OVER_RETRY_PARKING = undefined
    // EMERGENCY_PULL_OVER_SLOW_DOWN = undefined
    // EMERGENCY_PULL_OVER_APPROACH = undefined
    // EMERGENCY_PULL_OVER_STANDBY = undefined
    // EMERGENCY_STOP_APPROACH = undefined
    // EMERGENCY_STOP_STANDBY = undefined
    // VALET_PARKING_APPROACHING_PARKING_SPOT = undefined
    // VALET_PARKING_PARKING = undefined
    // DEADEND_TURNAROUND_APPROACHING_TURNING_POINT = undefined
    // DEADEND_TURNAROUND_TURNING = undefined
    // PARK_AND_GO_CHECK = undefined
    // PARK_AND_GO_CRUISE = undefined
    // PARK_AND_GO_ADJUST = undefined
    // PARK_AND_GO_PRE_CRUISE = undefined
    // YIELD_SIGN_APPROACH = undefined
    // YIELD_SIGN_CREEP = undefined
    // LEARNING_MODEL_RUN = undefined
    "stage_type": number,
    "msg": string,
  },
  "apollo.planning_internal.OpenSpaceDebug": {
    "trajectories": MessageTypeBySchemaName["apollo.planning_internal.Trajectories"],
    "warm_start_trajectory": MessageTypeBySchemaName["apollo.common.VehicleMotion"],
    "smoothed_trajectory": MessageTypeBySchemaName["apollo.common.VehicleMotion"],
    "warm_start_dual_lambda": Float64Array,
    "warm_start_dual_miu": Float64Array,
    "optimized_dual_lambda": Float64Array,
    "optimized_dual_miu": Float64Array,
    "xy_boundary": Float64Array,
    "obstacles": MessageTypeBySchemaName["apollo.planning_internal.ObstacleDebug"][],
    "roi_shift_point": MessageTypeBySchemaName["apollo.common.TrajectoryPoint"],
    "end_point": MessageTypeBySchemaName["apollo.common.TrajectoryPoint"],
    "partitioned_trajectories": MessageTypeBySchemaName["apollo.planning_internal.Trajectories"],
    "chosen_trajectory": MessageTypeBySchemaName["apollo.planning_internal.Trajectories"],
    "is_fallback_trajectory": boolean,
    "fallback_trajectory": MessageTypeBySchemaName["apollo.planning_internal.Trajectories"],
    "trajectory_stitching_point": MessageTypeBySchemaName["apollo.common.TrajectoryPoint"],
    "future_collision_point": MessageTypeBySchemaName["apollo.common.TrajectoryPoint"],
    "time_latency": number,
    "origin_point": MessageTypeBySchemaName["apollo.common.PointENU"],
    "origin_heading_rad": number,
  },
  "apollo.planning_internal.Trajectories": {
    "trajectory": MessageTypeBySchemaName["apollo.common.Trajectory"][],
  },
  "apollo.common.Trajectory": {
    "name": string,
    "trajectory_point": MessageTypeBySchemaName["apollo.common.TrajectoryPoint"][],
  },
  "apollo.common.VehicleMotion": {
    "name": string,
    "vehicle_motion_point": MessageTypeBySchemaName["apollo.common.VehicleMotionPoint"][],
  },
  "apollo.common.VehicleMotionPoint": {
    "trajectory_point": MessageTypeBySchemaName["apollo.common.TrajectoryPoint"],
    "steer": number,
  },
  "apollo.planning_internal.SmootherDebug": {
    "is_smoothed": boolean,
    // SMOOTHER_NONE = undefined
    // SMOOTHER_CLOSE_STOP = undefined
    "type": number,
    "reason": string,
  },
  "apollo.planning_internal.PullOverDebug": {
    "position": MessageTypeBySchemaName["apollo.common.PointENU"],
    "theta": number,
    "length_front": number,
    "length_back": number,
    "width_left": number,
    "width_right": number,
  },
  "apollo.planning_internal.HybridModelDebug": {
    "using_learning_model_output": boolean,
    "learning_model_output_usage_ratio": number,
    "learning_model_output_fail_reason": string,
    "evaluated_path_reference": MessageTypeBySchemaName["apollo.common.Path"],
  },
  "apollo.planning.DecisionResult": {
    "main_decision": MessageTypeBySchemaName["apollo.planning.MainDecision"],
    "object_decision": MessageTypeBySchemaName["apollo.planning.ObjectDecisions"],
    "vehicle_signal": MessageTypeBySchemaName["apollo.common.VehicleSignal"],
  },
  "apollo.planning.MainDecision": {
    "cruise": MessageTypeBySchemaName["apollo.planning.MainCruise"],
    "stop": MessageTypeBySchemaName["apollo.planning.MainStop"],
    "estop": MessageTypeBySchemaName["apollo.planning.MainEmergencyStop"],
    "change_lane": MessageTypeBySchemaName["apollo.planning.MainChangeLane"],
    "mission_complete": MessageTypeBySchemaName["apollo.planning.MainMissionComplete"],
    "not_ready": MessageTypeBySchemaName["apollo.planning.MainNotReady"],
    "parking": MessageTypeBySchemaName["apollo.planning.MainParking"],
    "target_lane": MessageTypeBySchemaName["apollo.planning.TargetLane"][],
  },
  "apollo.planning.MainCruise": {
    // FORWARD = undefined
    // LEFT = undefined
    // RIGHT = undefined
    "change_lane_type": number,
  },
  "apollo.planning.MainStop": {
    // STOP_REASON_HEAD_VEHICLE = undefined
    // STOP_REASON_DESTINATION = undefined
    // STOP_REASON_PEDESTRIAN = undefined
    // STOP_REASON_OBSTACLE = undefined
    // STOP_REASON_PREPARKING = undefined
    // STOP_REASON_SIGNAL = undefined
    // STOP_REASON_STOP_SIGN = undefined
    // STOP_REASON_YIELD_SIGN = undefined
    // STOP_REASON_CLEAR_ZONE = undefined
    // STOP_REASON_CROSSWALK = undefined
    // STOP_REASON_CREEPER = undefined
    // STOP_REASON_REFERENCE_END = undefined
    // STOP_REASON_YELLOW_SIGNAL = undefined
    // STOP_REASON_PULL_OVER = undefined
    // STOP_REASON_SIDEPASS_SAFETY = undefined
    // STOP_REASON_PRE_OPEN_SPACE_STOP = undefined
    // STOP_REASON_LANE_CHANGE_URGENCY = undefined
    // STOP_REASON_EMERGENCY = undefined
    "reason_code": number,
    "reason": string,
    "stop_point": MessageTypeBySchemaName["apollo.common.PointENU"],
    "stop_heading": number,
    // FORWARD = undefined
    // LEFT = undefined
    // RIGHT = undefined
    "change_lane_type": number,
  },
  "apollo.planning.MainEmergencyStop": {
    // ESTOP_REASON_INTERNAL_ERR = undefined
    // ESTOP_REASON_COLLISION = undefined
    // ESTOP_REASON_ST_FIND_PATH = undefined
    // ESTOP_REASON_ST_MAKE_DECISION = undefined
    // ESTOP_REASON_SENSOR_ERROR = undefined
    "reason_code": number,
    "reason": string,
    "hard_brake": MessageTypeBySchemaName["apollo.planning.EmergencyStopHardBrake"],
    "cruise_to_stop": MessageTypeBySchemaName["apollo.planning.EmergencyStopCruiseToStop"],
  },
  "apollo.planning.EmergencyStopHardBrake": {
  },
  "apollo.planning.EmergencyStopCruiseToStop": {
  },
  "apollo.planning.MainChangeLane": {
    // LEFT = undefined
    // RIGHT = undefined
    "type": number,
    "default_lane": MessageTypeBySchemaName["apollo.planning.TargetLane"][],
    "default_lane_stop": MessageTypeBySchemaName["apollo.planning.MainStop"],
    "target_lane_stop": MessageTypeBySchemaName["apollo.planning.MainStop"],
  },
  "apollo.planning.TargetLane": {
    "id": string,
    "start_s": number,
    "end_s": number,
    "speed_limit": number,
  },
  "apollo.planning.MainMissionComplete": {
    "stop_point": MessageTypeBySchemaName["apollo.common.PointENU"],
    "stop_heading": number,
  },
  "apollo.planning.MainNotReady": {
    "reason": string,
  },
  "apollo.planning.MainParking": {
    // IN_PARKING = undefined
    "status": number,
  },
  "apollo.planning.ObjectDecisions": {
    "decision": MessageTypeBySchemaName["apollo.planning.ObjectDecision"][],
  },
  "apollo.planning.ObjectDecision": {
    "id": string,
    "perception_id": number,
    "object_decision": MessageTypeBySchemaName["apollo.planning.ObjectDecisionType"][],
  },
  "apollo.planning.LatencyStats": {
    "total_time_ms": number,
    "task_stats": MessageTypeBySchemaName["apollo.planning.TaskStats"][],
    "init_frame_time_ms": number,
  },
  "apollo.planning.TaskStats": {
    "name": string,
    "time_ms": number,
  },
  "apollo.planning.ADCTrajectory.CriticalRegion": {
    "region": MessageTypeBySchemaName["apollo.common.Polygon"][],
  },
  "apollo.common.Polygon": {
    "point": MessageTypeBySchemaName["apollo.common.Point3D"][],
  },
  "apollo.planning.RSSInfo": {
    "is_rss_safe": boolean,
    "cur_dist_lon": number,
    "rss_safe_dist_lon": number,
    "acc_lon_range_minimum": number,
    "acc_lon_range_maximum": number,
    "acc_lat_left_range_minimum": number,
    "acc_lat_left_range_maximum": number,
    "acc_lat_right_range_minimum": number,
    "acc_lat_right_range_maximum": number,
  },
  "apollo.prediction.PredictionObstacles": {
    "header": MessageTypeBySchemaName["apollo.common.Header"],
    "prediction_obstacle": MessageTypeBySchemaName["apollo.prediction.PredictionObstacle"][],
    // OK = undefined
    // CONTROL_ERROR = undefined
    // CONTROL_INIT_ERROR = undefined
    // CONTROL_COMPUTE_ERROR = undefined
    // CONTROL_ESTOP_ERROR = undefined
    // PERFECT_CONTROL_ERROR = undefined
    // CANBUS_ERROR = undefined
    // CAN_CLIENT_ERROR_BASE = undefined
    // CAN_CLIENT_ERROR_OPEN_DEVICE_FAILED = undefined
    // CAN_CLIENT_ERROR_FRAME_NUM = undefined
    // CAN_CLIENT_ERROR_SEND_FAILED = undefined
    // CAN_CLIENT_ERROR_RECV_FAILED = undefined
    // LOCALIZATION_ERROR = undefined
    // LOCALIZATION_ERROR_MSG = undefined
    // LOCALIZATION_ERROR_LIDAR = undefined
    // LOCALIZATION_ERROR_INTEG = undefined
    // LOCALIZATION_ERROR_GNSS = undefined
    // PERCEPTION_ERROR = undefined
    // PERCEPTION_ERROR_TF = undefined
    // PERCEPTION_ERROR_PROCESS = undefined
    // PERCEPTION_FATAL = undefined
    // PERCEPTION_ERROR_NONE = undefined
    // PERCEPTION_ERROR_UNKNOWN = undefined
    // PREDICTION_ERROR = undefined
    // PLANNING_ERROR = undefined
    // PLANNING_ERROR_NOT_READY = undefined
    // HDMAP_DATA_ERROR = undefined
    // ROUTING_ERROR = undefined
    // ROUTING_ERROR_REQUEST = undefined
    // ROUTING_ERROR_RESPONSE = undefined
    // ROUTING_ERROR_NOT_READY = undefined
    // END_OF_INPUT = undefined
    // HTTP_LOGIC_ERROR = undefined
    // HTTP_RUNTIME_ERROR = undefined
    // RELATIVE_MAP_ERROR = undefined
    // RELATIVE_MAP_NOT_READY = undefined
    // DRIVER_ERROR_GNSS = undefined
    // DRIVER_ERROR_VELODYNE = undefined
    // STORYTELLING_ERROR = undefined
    "perception_error_code": number,
    "start_timestamp": number,
    "end_timestamp": number,
    "intent": MessageTypeBySchemaName["apollo.prediction.Intent"],
    "scenario": MessageTypeBySchemaName["apollo.prediction.Scenario"],
  },
  "apollo.prediction.PredictionObstacle": {
    "perception_obstacle": MessageTypeBySchemaName["apollo.perception.PerceptionObstacle"],
    "timestamp": number,
    "predicted_period": number,
    "trajectory": MessageTypeBySchemaName["apollo.prediction.Trajectory"][],
    "intent": MessageTypeBySchemaName["apollo.prediction.ObstacleIntent"],
    "priority": MessageTypeBySchemaName["apollo.prediction.ObstaclePriority"],
    "interactive_tag": MessageTypeBySchemaName["apollo.prediction.ObstacleInteractiveTag"],
    "is_static": boolean,
    "feature": MessageTypeBySchemaName["apollo.prediction.Feature"][],
  },
  "apollo.perception.PerceptionObstacle": {
    "id": number,
    "position": MessageTypeBySchemaName["apollo.common.Point3D"],
    "theta": number,
    "velocity": MessageTypeBySchemaName["apollo.common.Point3D"],
    "length": number,
    "width": number,
    "height": number,
    "polygon_point": MessageTypeBySchemaName["apollo.common.Point3D"][],
    "tracking_time": number,
    // UNKNOWN = undefined
    // UNKNOWN_MOVABLE = undefined
    // UNKNOWN_UNMOVABLE = undefined
    // PEDESTRIAN = undefined
    // BICYCLE = undefined
    // VEHICLE = undefined
    "type": number,
    "timestamp": number,
    "point_cloud": Float64Array,
    "confidence": number,
    // CONFIDENCE_UNKNOWN = undefined
    // CONFIDENCE_CNN = undefined
    // CONFIDENCE_RADAR = undefined
    "confidence_type": number,
    "drops": MessageTypeBySchemaName["apollo.common.Point3D"][],
    "acceleration": MessageTypeBySchemaName["apollo.common.Point3D"],
    "anchor_point": MessageTypeBySchemaName["apollo.common.Point3D"],
    "bbox2d": MessageTypeBySchemaName["apollo.perception.BBox2D"],
    // ST_UNKNOWN = undefined
    // ST_UNKNOWN_MOVABLE = undefined
    // ST_UNKNOWN_UNMOVABLE = undefined
    // ST_CAR = undefined
    // ST_VAN = undefined
    // ST_TRUCK = undefined
    // ST_BUS = undefined
    // ST_CYCLIST = undefined
    // ST_MOTORCYCLIST = undefined
    // ST_TRICYCLIST = undefined
    // ST_PEDESTRIAN = undefined
    // ST_TRAFFICCONE = undefined
    "sub_type": number,
    "measurements": MessageTypeBySchemaName["apollo.perception.SensorMeasurement"][],
    "height_above_ground": number,
    "position_covariance": Float64Array,
    "velocity_covariance": Float64Array,
    "acceleration_covariance": Float64Array,
    "light_status": MessageTypeBySchemaName["apollo.perception.LightStatus"],
    "msg": MessageTypeBySchemaName["apollo.perception.DebugMessage"],
    // HOST_VEHICLE = undefined
    // V2X = undefined
    "source": number,
    "v2x_info": MessageTypeBySchemaName["apollo.perception.V2XInformation"],
  },
  "apollo.perception.BBox2D": {
    "xmin": number,
    "ymin": number,
    "xmax": number,
    "ymax": number,
  },
  "apollo.perception.SensorMeasurement": {
    "sensor_id": string,
    "id": number,
    "position": MessageTypeBySchemaName["apollo.common.Point3D"],
    "theta": number,
    "length": number,
    "width": number,
    "height": number,
    "velocity": MessageTypeBySchemaName["apollo.common.Point3D"],
    // UNKNOWN = undefined
    // UNKNOWN_MOVABLE = undefined
    // UNKNOWN_UNMOVABLE = undefined
    // PEDESTRIAN = undefined
    // BICYCLE = undefined
    // VEHICLE = undefined
    "type": number,
    // ST_UNKNOWN = undefined
    // ST_UNKNOWN_MOVABLE = undefined
    // ST_UNKNOWN_UNMOVABLE = undefined
    // ST_CAR = undefined
    // ST_VAN = undefined
    // ST_TRUCK = undefined
    // ST_BUS = undefined
    // ST_CYCLIST = undefined
    // ST_MOTORCYCLIST = undefined
    // ST_TRICYCLIST = undefined
    // ST_PEDESTRIAN = undefined
    // ST_TRAFFICCONE = undefined
    "sub_type": number,
    "timestamp": number,
    "box": MessageTypeBySchemaName["apollo.perception.BBox2D"],
  },
  "apollo.perception.LightStatus": {
    "brake_visible": number,
    "brake_switch_on": number,
    "left_turn_visible": number,
    "left_turn_switch_on": number,
    "right_turn_visible": number,
    "right_turn_switch_on": number,
  },
  "apollo.perception.DebugMessage": {
    "trajectory": MessageTypeBySchemaName["apollo.perception.Trajectory"][],
  },
  "apollo.perception.Trajectory": {
    "probability": number,
    "trajectory_point": MessageTypeBySchemaName["apollo.common.TrajectoryPoint"][],
  },
  "apollo.perception.V2XInformation": {
    // NONE = undefined
    // ZOMBIES_CAR = undefined
    // BLIND_ZONE = undefined
    "v2x_type": number,
  },
  "apollo.prediction.Trajectory": {
    "probability": number,
    "trajectory_point": MessageTypeBySchemaName["apollo.common.TrajectoryPoint"][],
  },
  "apollo.prediction.ObstacleIntent": {
    // UNKNOWN = undefined
    // STOP = undefined
    // STATIONARY = undefined
    // MOVING = undefined
    // CHANGE_LANE = undefined
    // LOW_ACCELERATION = undefined
    // HIGH_ACCELERATION = undefined
    // LOW_DECELERATION = undefined
    // HIGH_DECELERATION = undefined
    "type": number,
  },
  "apollo.prediction.ObstaclePriority": {
    // CAUTION = undefined
    // NORMAL = undefined
    // IGNORE = undefined
    "priority": number,
  },
  "apollo.prediction.ObstacleInteractiveTag": {
    // INTERACTION = undefined
    // NONINTERACTION = undefined
    "interactive_tag": number,
  },
  "apollo.prediction.Feature": {
    "id": number,
    "polygon_point": MessageTypeBySchemaName["apollo.common.Point3D"][],
    "position": MessageTypeBySchemaName["apollo.common.Point3D"],
    "front_position": MessageTypeBySchemaName["apollo.common.Point3D"],
    "velocity": MessageTypeBySchemaName["apollo.common.Point3D"],
    "raw_velocity": MessageTypeBySchemaName["apollo.common.Point3D"],
    "acceleration": MessageTypeBySchemaName["apollo.common.Point3D"],
    "velocity_heading": number,
    "speed": number,
    "acc": number,
    "theta": number,
    "length": number,
    "width": number,
    "height": number,
    "tracking_time": number,
    "timestamp": number,
    "lane": MessageTypeBySchemaName["apollo.prediction.Lane"],
    "junction_feature": MessageTypeBySchemaName["apollo.prediction.JunctionFeature"],
    "t_position": MessageTypeBySchemaName["apollo.common.Point3D"],
    "t_velocity": MessageTypeBySchemaName["apollo.common.Point3D"],
    "t_velocity_heading": number,
    "t_speed": number,
    "t_acceleration": MessageTypeBySchemaName["apollo.common.Point3D"],
    "t_acc": number,
    "is_still": boolean,
    // UNKNOWN = undefined
    // UNKNOWN_MOVABLE = undefined
    // UNKNOWN_UNMOVABLE = undefined
    // PEDESTRIAN = undefined
    // BICYCLE = undefined
    // VEHICLE = undefined
    "type": number,
    "label_update_time_delta": number,
    "priority": MessageTypeBySchemaName["apollo.prediction.ObstaclePriority"],
    "interactive_tag": MessageTypeBySchemaName["apollo.prediction.ObstacleInteractiveTag"],
    "is_near_junction": boolean,
    "future_trajectory_points": MessageTypeBySchemaName["apollo.prediction.PredictionTrajectoryPoint"][],
    "short_term_predicted_trajectory_points": MessageTypeBySchemaName["apollo.common.TrajectoryPoint"][],
    "predicted_trajectory": MessageTypeBySchemaName["apollo.prediction.Trajectory"][],
    "adc_trajectory_point": MessageTypeBySchemaName["apollo.common.TrajectoryPoint"][],
    "adc_timestamp": number,
    "adc_localization": MessageTypeBySchemaName["apollo.perception.PerceptionObstacle"],
    "surrounding_lane_id": string[],
    "within_lane_id": string[],
  },
  "apollo.prediction.Lane": {
    "current_lane_feature": MessageTypeBySchemaName["apollo.prediction.LaneFeature"][],
    "lane_feature": MessageTypeBySchemaName["apollo.prediction.LaneFeature"],
    "nearby_lane_feature": MessageTypeBySchemaName["apollo.prediction.LaneFeature"][],
    "lane_graph": MessageTypeBySchemaName["apollo.prediction.LaneGraph"],
    "lane_graph_ordered": MessageTypeBySchemaName["apollo.prediction.LaneGraph"],
    "label_update_time_delta": number,
  },
  "apollo.prediction.LaneFeature": {
    "lane_id": string,
    "lane_turn_type": number,
    "lane_s": number,
    "lane_l": number,
    "angle_diff": number,
    "dist_to_left_boundary": number,
    "dist_to_right_boundary": number,
    "lane_heading": number,
    // NONE = undefined
    // CITY_DRIVING = undefined
    // BIKING = undefined
    // SIDEWALK = undefined
    // PARKING = undefined
    // SHOULDER = undefined
    "lane_type": number,
  },
  "apollo.prediction.LaneGraph": {
    "lane_sequence": MessageTypeBySchemaName["apollo.prediction.LaneSequence"][],
  },
  "apollo.prediction.LaneSequence": {
    "lane_sequence_id": number,
    "lane_segment": MessageTypeBySchemaName["apollo.prediction.LaneSegment"][],
    "adc_lane_segment_idx": number,
    "path_point": MessageTypeBySchemaName["apollo.common.PathPoint"][],
    // NONE = undefined
    // CITY_DRIVING = undefined
    // BIKING = undefined
    // SIDEWALK = undefined
    // PARKING = undefined
    // SHOULDER = undefined
    "lane_type": number,
    "lane_s": number,
    "lane_l": number,
    "vehicle_on_lane": boolean,
    "nearby_obstacle": MessageTypeBySchemaName["apollo.prediction.NearbyObstacle"][],
    "stop_sign": MessageTypeBySchemaName["apollo.prediction.StopSign"],
    "right_of_way": number,
    "features": MessageTypeBySchemaName["apollo.prediction.LaneSequence.Features"],
    "label": number,
    "probability": number,
    "acceleration": number,
    "time_to_lane_center": number,
    "time_to_lane_edge": number,
    // NOT_GOTO_LANE = undefined
    // CONSTANT_SPEED = undefined
    // SMALL_ACCELERATION = undefined
    // LARGE_ACCELERATION = undefined
    // SMALL_DECELERATION = undefined
    // LARGE_DECELERATION = undefined
    "behavior_type": number,
    "curr_lane_point": MessageTypeBySchemaName["apollo.prediction.LanePoint"][],
    "left_neighbor_point": MessageTypeBySchemaName["apollo.prediction.LanePoint"][],
    "right_neighbor_point": MessageTypeBySchemaName["apollo.prediction.LanePoint"][],
    "left_nearby_obstacle": MessageTypeBySchemaName["apollo.prediction.NearbyObstacle"][],
    "right_nearby_obstacle": MessageTypeBySchemaName["apollo.prediction.NearbyObstacle"][],
  },
  "apollo.prediction.LaneSegment": {
    "lane_id": string,
    "start_s": number,
    "end_s": number,
    "lane_turn_type": number,
    "lane_point": MessageTypeBySchemaName["apollo.prediction.LanePoint"][],
    "adc_s": number,
    "adc_lane_point_idx": number,
    "total_length": number,
  },
  "apollo.prediction.LanePoint": {
    "position": MessageTypeBySchemaName["apollo.common.Point3D"],
    "heading": number,
    "width": number,
    "relative_s": number,
    "relative_l": number,
    "angle_diff": number,
    "kappa": number,
    // URBAN_ROAD = undefined
    // JUNCTION = undefined
    // HIGHWAY = undefined
    "scenario_type": number,
    "speed_limit": number,
  },
  "apollo.prediction.NearbyObstacle": {
    "id": number,
    "s": number,
    "l": number,
  },
  "apollo.prediction.StopSign": {
    "stop_sign_id": string,
    "lane_id": string,
    "lane_s": number,
    "lane_sequence_s": number,
  },
  "apollo.prediction.LaneSequence.Features": {
    "mlp_features": Float64Array,
  },
  "apollo.prediction.JunctionFeature": {
    "junction_id": string,
    "junction_range": number,
    "enter_lane": MessageTypeBySchemaName["apollo.prediction.LaneFeature"],
    "junction_exit": MessageTypeBySchemaName["apollo.prediction.JunctionExit"][],
    "junction_mlp_feature": Float64Array,
    "junction_mlp_label": Int32Array,
    "junction_mlp_probability": Float64Array,
    "start_lane_id": string[],
  },
  "apollo.prediction.JunctionExit": {
    "exit_lane_id": string,
    "exit_position": MessageTypeBySchemaName["apollo.common.Point3D"],
    "exit_heading": number,
    "exit_width": number,
  },
  "apollo.prediction.PredictionTrajectoryPoint": {
    "path_point": MessageTypeBySchemaName["apollo.prediction.PredictionPathPoint"],
    "timestamp": number,
  },
  "apollo.prediction.PredictionPathPoint": {
    "x": number,
    "y": number,
    "velocity_heading": number,
  },
  "apollo.prediction.Intent": {
    // UNKNOWN = undefined
    // STOP = undefined
    // CRUISE = undefined
    // CHANGE_LANE = undefined
    "type": number,
  },
  "apollo.prediction.Scenario": {
    // UNKNOWN = undefined
    // CRUISE = undefined
    // CRUISE_URBAN = undefined
    // CRUISE_HIGHWAY = undefined
    // JUNCTION = undefined
    // JUNCTION_TRAFFIC_LIGHT = undefined
    // JUNCTION_STOP_SIGN = undefined
    "type": number,
    "junction_id": string,
  },
  "google.protobuf.Timestamp": {
    "sec": bigint,
    "nsec": number,
  },
  "apollo.drivers.gnss.GnssBestPose": {
    "header": MessageTypeBySchemaName["apollo.common.Header"],
    "measurement_time": number,
    // SOL_COMPUTED = undefined
    // INSUFFICIENT_OBS = undefined
    // NO_CONVERGENCE = undefined
    // SINGULARITY = undefined
    // COV_TRACE = undefined
    // TEST_DIST = undefined
    // COLD_START = undefined
    // V_H_LIMIT = undefined
    // VARIANCE = undefined
    // RESIDUALS = undefined
    // INTEGRITY_WARNING = undefined
    // PENDING = undefined
    // INVALID_FIX = undefined
    // UNAUTHORIZED = undefined
    // INVALID_RATE = undefined
    "sol_status": number,
    // NONE = undefined
    // FIXEDPOS = undefined
    // FIXEDHEIGHT = undefined
    // FLOATCONV = undefined
    // WIDELANE = undefined
    // NARROWLANE = undefined
    // DOPPLER_VELOCITY = undefined
    // SINGLE = undefined
    // PSRDIFF = undefined
    // WAAS = undefined
    // PROPOGATED = undefined
    // OMNISTAR = undefined
    // L1_FLOAT = undefined
    // IONOFREE_FLOAT = undefined
    // NARROW_FLOAT = undefined
    // L1_INT = undefined
    // WIDE_INT = undefined
    // NARROW_INT = undefined
    // RTK_DIRECT_INS = undefined
    // INS_SBAS = undefined
    // INS_PSRSP = undefined
    // INS_PSRDIFF = undefined
    // INS_RTKFLOAT = undefined
    // INS_RTKFIXED = undefined
    // INS_OMNISTAR = undefined
    // INS_OMNISTAR_HP = undefined
    // INS_OMNISTAR_XP = undefined
    // OMNISTAR_HP = undefined
    // OMNISTAR_XP = undefined
    // PPP_CONVERGING = undefined
    // PPP = undefined
    // INS_PPP_CONVERGING = undefined
    // INS_PPP = undefined
    "sol_type": number,
    "latitude": number,
    "longitude": number,
    "height_msl": number,
    "undulation": number,
    // WGS84 = undefined
    "datum_id": number,
    "latitude_std_dev": number,
    "longitude_std_dev": number,
    "height_std_dev": number,
    "base_station_id": Uint8Array,
    "differential_age": number,
    "solution_age": number,
    "num_sats_tracked": number,
    "num_sats_in_solution": number,
    "num_sats_l1": number,
    "num_sats_multi": number,
    "reserved": number,
    "extended_solution_status": number,
    "galileo_beidou_used_mask": number,
    "gps_glonass_used_mask": number,
  },
  "apollo.localization.CorrectedImu": {
    "header": MessageTypeBySchemaName["apollo.common.Header"],
    "imu": MessageTypeBySchemaName["apollo.localization.Pose"],
  },
  "apollo.drivers.gnss.GnssStatus": {
    "header": MessageTypeBySchemaName["apollo.common.Header"],
    "solution_completed": boolean,
    "solution_status": number,
    "position_type": number,
    "num_sats": number,
  },
  "apollo.drivers.gnss.Imu": {
    "header": MessageTypeBySchemaName["apollo.common.Header"],
    "measurement_time": number,
    "measurement_span": number,
    "linear_acceleration": MessageTypeBySchemaName["apollo.common.Point3D"],
    "angular_velocity": MessageTypeBySchemaName["apollo.common.Point3D"],
  },
  "apollo.drivers.gnss.InsStatus": {
    "header": MessageTypeBySchemaName["apollo.common.Header"],
    // INVALID = undefined
    // CONVERGING = undefined
    // GOOD = undefined
    "type": number,
  },
  "apollo.localization.Gps": {
    "header": MessageTypeBySchemaName["apollo.common.Header"],
    "localization": MessageTypeBySchemaName["apollo.localization.Pose"],
  },
  "apollo.transform.TransformStampeds": {
    "header": MessageTypeBySchemaName["apollo.common.Header"],
    "transforms": MessageTypeBySchemaName["apollo.transform.TransformStamped"][],
  },
  "apollo.transform.TransformStamped": {
    "header": MessageTypeBySchemaName["apollo.common.Header"],
    "child_frame_id": string,
    "transform": MessageTypeBySchemaName["apollo.transform.Transform"],
  },
  "apollo.transform.Transform": {
    "translation": MessageTypeBySchemaName["apollo.common.Point3D"],
    "rotation": MessageTypeBySchemaName["apollo.common.Quaternion"],
  },
  "apollo.perception.PerceptionObstacles": {
    "perception_obstacle": MessageTypeBySchemaName["apollo.perception.PerceptionObstacle"][],
    "header": MessageTypeBySchemaName["apollo.common.Header"],
    // OK = undefined
    // CONTROL_ERROR = undefined
    // CONTROL_INIT_ERROR = undefined
    // CONTROL_COMPUTE_ERROR = undefined
    // CONTROL_ESTOP_ERROR = undefined
    // PERFECT_CONTROL_ERROR = undefined
    // CANBUS_ERROR = undefined
    // CAN_CLIENT_ERROR_BASE = undefined
    // CAN_CLIENT_ERROR_OPEN_DEVICE_FAILED = undefined
    // CAN_CLIENT_ERROR_FRAME_NUM = undefined
    // CAN_CLIENT_ERROR_SEND_FAILED = undefined
    // CAN_CLIENT_ERROR_RECV_FAILED = undefined
    // LOCALIZATION_ERROR = undefined
    // LOCALIZATION_ERROR_MSG = undefined
    // LOCALIZATION_ERROR_LIDAR = undefined
    // LOCALIZATION_ERROR_INTEG = undefined
    // LOCALIZATION_ERROR_GNSS = undefined
    // PERCEPTION_ERROR = undefined
    // PERCEPTION_ERROR_TF = undefined
    // PERCEPTION_ERROR_PROCESS = undefined
    // PERCEPTION_FATAL = undefined
    // PERCEPTION_ERROR_NONE = undefined
    // PERCEPTION_ERROR_UNKNOWN = undefined
    // PREDICTION_ERROR = undefined
    // PLANNING_ERROR = undefined
    // PLANNING_ERROR_NOT_READY = undefined
    // HDMAP_DATA_ERROR = undefined
    // ROUTING_ERROR = undefined
    // ROUTING_ERROR_REQUEST = undefined
    // ROUTING_ERROR_RESPONSE = undefined
    // ROUTING_ERROR_NOT_READY = undefined
    // END_OF_INPUT = undefined
    // HTTP_LOGIC_ERROR = undefined
    // HTTP_RUNTIME_ERROR = undefined
    // RELATIVE_MAP_ERROR = undefined
    // RELATIVE_MAP_NOT_READY = undefined
    // DRIVER_ERROR_GNSS = undefined
    // DRIVER_ERROR_VELODYNE = undefined
    // STORYTELLING_ERROR = undefined
    "error_code": number,
    "lane_marker": MessageTypeBySchemaName["apollo.perception.LaneMarkers"],
    "cipv_info": MessageTypeBySchemaName["apollo.perception.CIPVInfo"],

  },
  "apollo.perception.CIPVInfo": {
    "cipv_id": number,
    "potential_cipv_id": Int32Array,
  },
};
