///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

import { Message } from "./apolloTypes";

import { FrameTransforms, Quaternion } from "@foxglove/schemas";
import { quaternionFromZAngle } from "./common";

// For world -> localization TF we keep only yaw angle, as in
// Perception, prediciton and Planning modules roll and pitch
// are ignored
function removeXYRotation(
  msg: Message<"apollo.common.Quaternion">
): Quaternion {
  let yaw = Math.atan2(
    2.0 * (msg.qw * msg.qz - msg.qx * msg.qy),
    2 * (msg.qw * msg.qw + msg.qy * msg.qy) - 1.0
  );
  return quaternionFromZAngle(yaw);
}

export function convertTransforms(
  msg: Message<"apollo.transform.TransformStampeds">
): FrameTransforms {
  let out_message: FrameTransforms = <FrameTransforms>{};
  out_message.transforms = [];

  for (let apollo_tr of msg.transforms) {
    let ts = apollo_tr?.header?.timestamp_sec || msg.header.timestamp_sec;

    out_message.transforms.push({
      timestamp: {
        sec: Math.floor(ts),
        nsec: ((ts % 1) * 10) ^ 9,
      },
      child_frame_id: apollo_tr.child_frame_id,
      parent_frame_id: apollo_tr.header.frame_id,
      rotation:
        apollo_tr.header.frame_id === "world"
          ? removeXYRotation(apollo_tr.transform.rotation)
          : {
              x: apollo_tr.transform.rotation.qx,
              y: apollo_tr.transform.rotation.qy,
              z: apollo_tr.transform.rotation.qz,
              w: apollo_tr.transform.rotation.qw,
            },
      translation: apollo_tr.transform.translation,
    });
  }
  return out_message;
}
