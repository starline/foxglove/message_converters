///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

import { Message, Time } from "./apolloTypes";
import {
  SceneEntity,
  LinePrimitive,
  Point3,
  SceneUpdate,
} from "@foxglove/schemas";

var COLOR = { r: 0.36, g: 1, b: 1, a: 0.5 };
var THICKNESS = 0.3;

function convertPoints(
  msg: Message<"apollo.planning.ADCTrajectory">
): SceneEntity {
  let ts = {
    sec: Math.round(msg.header.timestamp_sec),
    nsec: (msg.header.timestamp_sec % 1) * (10 ^ 9),
  };

  let line: LinePrimitive = <LinePrimitive>{};

  let adc_height = msg.debug.planning_data.adc_position.pose.position.z;

  let offset = msg.trajectory_point[0]?.path_point ?? <Point3>{};

  line.points = [];

  for (var traj_point of msg.trajectory_point) {
    line.points.push({
      x: traj_point.path_point.x - offset.x,
      y: traj_point.path_point.y - offset.y,
      z:
        (traj_point.path_point.z > 1e-7
          ? traj_point.path_point.z
          : adc_height) - offset.z,
    });
  }

  line.type = 0;
  line.pose = { position: offset, orientation: { x: 0, y: 0, z: 0, w: 1 } };

  line.thickness = THICKNESS;
  line.scale_invariant = false;
  line.color = COLOR;

  return {
    timestamp: ts,
    frame_id: "world",
    id: "0",
    lifetime: <Time>{},
    frame_locked: true,
    metadata: [],
    arrows: [],
    cubes: [],
    spheres: [],
    cylinders: [],
    lines: [line],
    triangles: [],
    texts: [],
    models: [],
  };
}

export function convertPlanning(
  msg: Message<"apollo.planning.ADCTrajectory">
): SceneUpdate {
  var entities: SceneEntity[] = [];

  entities.push(convertPoints(msg));

  return {
    deletions: [
      {
        timestamp: {
          sec: msg.header.timestamp_sec,
          nsec: ((msg.header.timestamp_sec % 1) * 10) ^ 9,
        },
        type: 1,
        id: "",
      },
    ],
    entities: entities,
  };
}
