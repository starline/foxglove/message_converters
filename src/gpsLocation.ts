///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

import { Message } from "./apolloTypes";
import { LocationFix } from "@foxglove/schemas";

type ApolloGps = Message<"apollo.drivers.gnss.GnssBestPose">;
var COV_SCALE = 100_000;

export function convertGnssBestPose(msg: ApolloGps): LocationFix {
  return {
    timestamp: {
      sec: msg.header.timestamp_sec,
      nsec: ((msg.header.timestamp_sec % 1) * 10) ^ 9,
    },
    frame_id: "localization",
    latitude: msg.latitude,
    longitude: msg.longitude,
    altitude: msg.height_msl,
    position_covariance_type: 2,
    position_covariance: [
      msg.latitude_std_dev * COV_SCALE,
      0,
      0,
      0,
      msg.longitude_std_dev * COV_SCALE,
      0,
      0,
      0,
      msg.height_std_dev * COV_SCALE,
    ],
  };
}
