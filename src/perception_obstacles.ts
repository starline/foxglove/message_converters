///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

import { Message } from "./apolloTypes";
import {
  Point2,
  Point3,
  LinePrimitive,
  TextPrimitive,
  SceneEntity,
  SceneUpdate,
  Color,
} from "@foxglove/schemas";

import { Time, quaternionFromZAngle } from "./common";

enum ObjectType {
  UNKNOWN = 0,
  UNKNOWN_MOVABLE = 1,
  UNKNOWN_UNMOVABLE = 2,
  PEDESTRIAN = 3, // Pedestrian, usually determined by moving behavior.
  BICYCLE = 4, // bike, motor bike
  VEHICLE = 5, // Passenger car or truck.
}

export function type2color(t: ObjectType): Color {
  switch (t) {
    case ObjectType.UNKNOWN:
    case ObjectType.UNKNOWN_MOVABLE:
    case ObjectType.UNKNOWN_UNMOVABLE:
      return { r: 1, g: 1, b: 1, a: 0.3 };
    case ObjectType.PEDESTRIAN:
      return { r: 0, g: 0, b: 1, a: 0.3 };
    case ObjectType.VEHICLE:
      return { r: 0, g: 1, b: 0, a: 0.3 };
    case ObjectType.BICYCLE:
      return { r: 1, g: 0, b: 0, a: 0.3 };
    default:
      return { r: 1, g: 0, b: 1, a: 0.3 };
  }
}

export function convertText(
  apollo_obj: Message<"apollo.perception.PerceptionObstacle">
): TextPrimitive {
  let velocity = Math.hypot(
    apollo_obj.velocity.x,
    apollo_obj.velocity.y,
    apollo_obj.velocity.z
  ).toFixed(1);

  let vel_str = velocity == "0.0" ? "" : `\nSpeed: ${velocity} m/s`;

  return {
    pose: {
      position: {
        x: apollo_obj.position.x,
        y: apollo_obj.position.y,
        // z: apollo_obj.position.z + apollo_obj.measurements[0].height + 0.5,
        z: apollo_obj.position.z + apollo_obj.height + 0.5,
      },
      orientation: quaternionFromZAngle(apollo_obj.theta),
    },
    billboard: true,
    font_size: 0.5,
    scale_invariant: false,
    color: { r: 1, g: 1, b: 1, a: 1 },
    text: `${ObjectType[apollo_obj.type]} ${
      apollo_obj.id
    } ${apollo_obj.tracking_time.toFixed(1)} sec${vel_str}`,
  };
}

// export function convertVelocityMarker(
//   apollo_obj: Message<"apollo.perception.PerceptionObstacle">, offset: Point3
// ): ArrowPrimitive {
//   let vel: number = Math.sqrt(apollo_obj.velocity.y ^ (2 + apollo_obj.velocity.x) ^ 2);

//   return {
//     color: type2color(apollo_obj.type),
//     pose: {
//       position: {
//         x: apollo_obj.position.x - offset.x,
//         y: apollo_obj.position.y - offset.y,
//         z: apollo_obj.position.z + apollo_obj.height / 2,
//       },
//       orientation: quaternionFromZAngle(Math.atan2(apollo_obj.velocity.y, apollo_obj.velocity.x)),
//     },
//     head_diameter: 0.3,
//     head_length: 0.2 * vel,
//     shaft_diameter: 0.2,
//     shaft_length: 0.8 * vel,
//   };
// }

export function convertContour(
  apollo_obj: Message<"apollo.perception.PerceptionObstacle">,
  offset: Point3
): LinePrimitive {
  let points: Point3[] = [];

  let prev_p: Point3 = {
    x:
      (apollo_obj.polygon_point[apollo_obj.polygon_point.length - 1]?.x ?? 0) -
      offset.x,
    y:
      (apollo_obj.polygon_point[apollo_obj.polygon_point.length - 1]?.y ?? 0) -
      offset.y,
    z: -offset.z,
  };

  let prev_p_up: Point3 = Object.assign({}, prev_p);

  let min_z = apollo_obj.polygon_point.reduce(function (prev, current) {
    return prev.z < current.z ? prev : current;
  }).z;
  let max_z = min_z + apollo_obj.height;

  prev_p.z = min_z - offset.z;
  prev_p_up.z = max_z - offset.z;

  let added_pts: Point2[] = [];

  for (let p of apollo_obj.polygon_point) {
    if (!added_pts.includes({ x: p.x, y: p.y })) {
      added_pts.push({ x: p.x, y: p.y });
    }
  }

  for (let p of added_pts) {
    let cur_p = { x: p.x - offset.x, y: p.y - offset.y, z: min_z - offset.z };
    let cur_p_up = {
      x: p.x - offset.x,
      y: p.y - offset.y,
      z: max_z - offset.z,
    };

    points.push(cur_p, cur_p_up);
    points.push(prev_p, cur_p);
    points.push(prev_p_up, cur_p_up);

    prev_p = cur_p;
    prev_p_up = cur_p_up;
  }

  let out_obj: LinePrimitive = <LinePrimitive>{};

  out_obj.type = 2;
  out_obj.pose = {
    position: offset,
    orientation: { x: 0, y: 0, z: 0, w: 1 },
  };
  out_obj.thickness = 0.2;
  out_obj.scale_invariant = false;
  out_obj.points = points;
  out_obj.color = type2color(apollo_obj.type);

  return out_obj;
}

export function convertPerceptionObstacle(
  apollo_obj: Message<"apollo.perception.PerceptionObstacle">,
  ts: Time,
  offset: Point3,
  frame_id: string
): SceneEntity {
  let entity: SceneEntity = <SceneEntity>{};

  entity.timestamp = ts;
  entity.texts = [convertText(apollo_obj)];
  entity.lines = [convertContour(apollo_obj, offset)];
  entity.metadata = [
    {
      key: "theta",
      value: String(apollo_obj.theta),
    },
  ];

  // entity.arrows = [convertVelocityMarker(apollo_obj, offset)];

  entity.frame_id = frame_id;
  entity.id = String(apollo_obj.id);

  entity.frame_locked = true;

  return entity;
}

export function convertTimestamp(apollo_ts: number): Time {
  return {
    sec: apollo_ts,
    nsec: (apollo_ts % 1) * (10 ^ 9),
  };
}

export function get_min_coordinates(
  apollo_obj: Message<"apollo.perception.PerceptionObstacle">
): Message<"foxglove.Point3"> {
  let min_x = apollo_obj.polygon_point.reduce((prev, cur) => {
    return prev.x < cur.x ? prev : cur;
  }).x;

  let min_y = apollo_obj.polygon_point.reduce((prev, cur) => {
    return prev.y < cur.y ? prev : cur;
  }).y;

  let min_z = apollo_obj.polygon_point.reduce((prev, cur) => {
    return prev.z < cur.z ? prev : cur;
  }).z;

  return { x: min_x, y: min_y, z: min_z };
}

export function convertPerceptionObstacles(
  message: Message<"apollo.perception.PerceptionObstacles">
): SceneUpdate {
  var entities: SceneEntity[] = [];

  var ts = convertTimestamp(message.header.timestamp_sec);

  if (message.header.frame_id == "") message.header.frame_id = "world";

  let offset: Point3 = <Point3>{};

  if (message.perception_obstacle[0]?.polygon_point != undefined) {
    offset = message.perception_obstacle[0].polygon_point[0] ?? <Point3>{};
  }

  for (var apollo_obj of message.perception_obstacle) {
    // let offset = get_min_coordinates(apollo_obj);

    let converted_obj = convertPerceptionObstacle(
      apollo_obj,
      ts,
      offset,
      message.header.frame_id
    );

    entities.push(converted_obj);
  }

  return {
    deletions: [
      {
        timestamp: ts,
        type: 1,
        id: "",
      },
    ],
    entities: entities,
  };
}
