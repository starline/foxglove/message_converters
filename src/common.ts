import { Quaternion } from "@foxglove/schemas";

export type Time = {
  sec: number;
  nsec: number;
};

export function quaternionFromZAngle(angle: number): Quaternion {
  return {
    x: 0,
    y: 0,
    z: Math.sin(angle / 2),
    w: Math.cos(angle / 2),
  };
}
