///////////////////////////////////////////////////////////////////////////////////
// Copyright 2023 ScPA StarLine Ltd. All Rights Reserved.
//
// Created by Ivan Nenakhov <nenakhov.id@starline.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
///////////////////////////////////////////////////////////////////////////////////

import { ExtensionContext } from "@foxglove/studio";

import { convertPerceptionObstacles } from "./perception_obstacles";
import { convertPredictionObstacles } from "./prediction_obstacles";
import { convertGnssBestPose } from "./gpsLocation";
import { convertTransforms } from "./transforms";
import { convertPlanning } from "./planning";

export function activate(extensionContext: ExtensionContext): void {
  extensionContext.registerMessageConverter({
    fromSchemaName: "apollo.drivers.gnss.GnssBestPose",
    toSchemaName: "foxglove.LocationFix",
    converter: convertGnssBestPose,
  });

  extensionContext.registerMessageConverter({
    fromSchemaName: "apollo.perception.PerceptionObstacles",
    toSchemaName: "foxglove.SceneUpdate",
    converter: convertPerceptionObstacles,
  });

  extensionContext.registerMessageConverter({
    fromSchemaName: "apollo.prediction.PredictionObstacles",
    toSchemaName: "foxglove.SceneUpdate",
    converter: convertPredictionObstacles,
  });

  extensionContext.registerMessageConverter({
    fromSchemaName: "apollo.transform.TransformStampeds",
    toSchemaName: "foxglove.FrameTransforms",
    converter: convertTransforms,
  });

  extensionContext.registerMessageConverter({
    fromSchemaName: "apollo.planning.ADCTrajectory",
    toSchemaName: "foxglove.SceneUpdate",
    converter: convertPlanning,
  });

}
