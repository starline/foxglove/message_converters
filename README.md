# Apollo-Foxglove Message Converters

[English version of README](README_EN.md)

Расширение преобразует типы данных из Apollo в формат foxglove для отображения на различных панелях:

 - обнаруженные объекты с Perception: ```apollo.perception.PerceptionObstacles``` -> ```foxglove.SceneUpdate```;
 - обнаруженные объекты с Prediction: ```apollo.perception.PredictionObstacles``` -> ```foxglove.SceneUpdate```;
 - спланированный путь: ```apollo.planning.ADCTrajectory``` -> ```foxglove.SceneUpdate```.
 - координаты GNSS: ```apollo.drivers.gnss.GnssBestPose``` -> ```foxglove.LocationFix```;
 - трансформации: ```apollo.transform.TransformStampeds``` -> ```foxglove.FrameTransforms```.

Конвертеры сообщений Apollo-Foxglove разрабатываются как часть проекта [Open Source Car (OSCAR)](https://gitlab.com/starline/oscar).

# Установка
1. Скачать релизный .foxe файл;
2. Перенести файл в папку foxglove:
   - Windows: запустить файл (распаковка и копирование произведутся автоматически)
   - Linux: распаковать архив: ```unzip starline.message_converters.foxe ~/.foxglove-studio/extensions/message_converters```

# Сборка

Для сборки конвертеров необходим Linux.

1. Установить node.js >=14 ([Инструкция](https://nodejs.org/en/download/package-manager));
2. Установить npm: ```sudo apt intall npm```;
3. Инициализировать проект: ```npm install```;
4. собрать и установить расширение в временную папку **~/.foxglove-studio** : ```npm run local-install```, где FoxGlove Studio хранит файлы установленных расширений. 

(Опционально) для сборки архива в директории проекта можно воспользоваться командой ```npm run package```.


## Особенности конвертеров

**!NB** Ввиду большого размера сообщений с облаками точек и изображениями, рекомендуется использовать их преобразование на стороне бриджа или конвертера записей.  

### Obstacles

Из-за использования UTM проекции координаты объектов в world фрейме могут принимать очень большие значения. Также в FoxGlove при работе с трансформациями точек используется Float32. Два этих факта приводят к тому, что происходит округление чисел с плавающей запятой, из-за чего на панели 3D координаты становятся некорректными. Для устранения этого эффекта вводится смещение для приближения координат точек к началу СК. Таким способом модифицируются полигоны препятствия, спрогнозированные пути (Prediction), траектория планирования, которые конвертируются в объекты ```LinePrimitive```. За смещение взяты координаты первой точки полигона первого препятствия. Вычтенные значения координат добавляются к полю ```pose.position```. 

### Transforms

Для трансформации между фреймами world и localization обнулены углы крена и тангажа, так как в основных модулях - Perception, Prediction  Planning они игнорируются.


[Issue по этой теме](https://github.com/foxglove/studio/issues/5737)

# Ссылки
 - [Foxglove](https://foxglove.dev)
