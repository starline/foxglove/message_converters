# Apollo-Foxglove Message Converters

This extension converts data types from Apollo into FoxGlove format to be viewed in various panels:

  - detected objects from Perception: ```apollo.perception.PerceptionObstacles``` -> ```foxglove.SceneUpdate```;
  - detected objects from Prediction: ```apollo.perception.PredictionObstacles``` -> ```foxglove.SceneUpdate```;
  - generated planning path: ```apollo.planning.ADCTrajectory``` -> ```foxglove.SceneUpdate```.
  - GNSS coordinates: ```apollo.drivers.gnss.GnssBestPose``` -> ```foxglove.LocationFix```;
  - transformations: ```apollo.transform.TransformStampeds``` -> ```foxglove.FrameTransforms```.

Apollo-Foxglove message converters are developed as a part of [Open Source Car (OSCAR) project](https://gitlab.com/starline/oscar).


# Installation
1. Download the release ```.foxe``` file ([last release](https://gitlab.com/starline/foxglove/message_converters/-/releases/permalink/latest/downloads/starline.message_converters.foxe));
2. Move the file to the foxglove folder:
    - Windows: run the file (unpacking and copying will be done automatically)
    - Linux: unzip archive: ```unzip starline.message_converters.foxe ~/.foxglove-studio/extensions/message_converters```

# Build

Linux is required to build the converters.

1. Install node.js >=14 ([Instructions](https://nodejs.org/en/download/package-manager));
2. Install npm: ```sudo apt intall npm```;
3. Initialize the project: ```npm install```;
4. install the extension in the temporary folder **~/.foxglove-studio** : ```npm run local-install```.


## Features of converters

**!NB** Because of large size of messages with point clouds and images, we desided to perform these conversions on th [bridge side](https://gitlab.com/starline/oscar_utils/foxglove/apollo_foxglove_support).

### Obstacles

Due to the use of UTM projection, the coordinates of objects in world frame can take on very large values. FoxGlove also uses Float32 when working with point transformations. These two facts lead to rounding of floating point numbers, which causes the 3D coordinates in the panel to become incorrect. To eliminate this effect, a shift is introduced to bring the coordinates of the points closer to the beginning of the coordinate frame. In this way, obstacle polygons, predicted paths (Prediction), and planning trajectory are modified, which are converted into ```LinePrimitive``` objects. The coordinates of the first point of the polygon of the first obstacle are taken as the offset. The subtracted coordinate values are added to the ```pose.position``` field.


[Corresponding issue](https://github.com/foxglove/studio/issues/5737)
